'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.accountsservices
 * @description
 * # accountsservices
 * Service in the joynrPromotersAngularApp.
 */
angular.module('AccountsServices', ['ngResource'])
        .service('Accounts', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/accounts/:action/:offset', {offset: '@offset'}, {
                    usersSearch: {method: 'POST', params: {action: 'usersSearch'}, isArray: true, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    usersSearchExact: {method: 'POST', params: {action: 'usersSearchExact'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);
