'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.discountsservice
 * @description
 * # discountsservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('discountsService', ['ngResource'])
        .factory('Discounts', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/discounts/:action/:id', {id:"@id"}, {
                    list: {method: 'GET', params: {action: 'getDiscounts'}, isArray: false},
                    add: {method: 'POST', params: {action: 'add'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    edit: {method: 'POST', params: {action: 'edit'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    remove: {method: 'POST', params: {action: 'remove'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);
