'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.dashboardservice
 * @description
 * # dashboardservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('DashboardService', ["dataservice"])
        .service('Dashboard', ["Data", "$filter", function (Data, $filter) {
                return{
                    "getTicketsTypeTableau": function (source) {

                        return Data.getTickets(source, false).reduce(function (res, obj) {
                            res.push({name: obj.name, y: obj.totalQuantity - obj.remainingQuantity});
                            res[0].y += obj.remainingQuantity;

                            return res;
                        }, [{name: 'Tickets left', y: 0, sliced: true, selected: true}]);
                    },
                    "getPieOrigin": function (data) {

                        var filtered = function (data, name, exist, add, res, obj) {
                            var index = data.map(function (o) {
                                if (o instanceof Array) {
                                    return o[0];
                                } else {
                                    return o.name;
                                }
                            }).indexOf(name);

                            if (index >= 0) {
                                exist(index, res, obj);
                            } else {
                                add(res, obj);
                            }
                        };

                        var exist = function (index, res, obj) {
                            res[0][index].y += 1;

                            filtered(res[1][index].data, obj.linklabel,
                                    function (pathNumber) {
                                        res[1][index].data[pathNumber][1] += 1;
                                    },
                                    function () {
                                        res[1][index].data.push([obj.linklabel, 1]);
                                    }
                            );

                        };

                        var add = function (res, obj) {
                            var name = $filter('linktype')(obj.linktype) || "direct";
                            var part = {
                                drilldown: name,
                                name: name,
                                visible: true,
                                y: 1
                            };

                            //we don't want a drilldown for direct access.
                            if (name == "direct") {
                                delete part.drilldown;
                            }

                            var drilldown = {id: name, name: name, data: []};
                            drilldown.data.push([obj.linklabel, 1]);
                            res[1].push(drilldown);
                            res[0].push(part);

                        };

                        var fnreduce = function (res, obj) {

                            var name = $filter('linktype')(obj.linktype) || "direct";

                            filtered(res[0], name, exist, add, res, obj);

                            return res;
                        };

                        return data.reduce(fnreduce, [[], []]);

                    },
                    "getPieOriginFromVisitors": function (data) {

                        var filtered = function (data, name, exist, add, res, obj) {
                            var index = data.map(function (o) {
                                if (o instanceof Array) {
                                    return o[0];
                                } else {
                                    return o.name;
                                }
                            }).indexOf(name);

                            if (index >= 0) {
                                exist(index, res, obj);
                            } else {
                                add(res, obj);
                            }
                        };

                        var exist = function (index, res, obj) {
                            res[0][index].y = res[0][index].y + obj.number;

                            filtered(res[1][index].data, obj.referer,
                                    function (pathNumber) {
                                        res[1][index].data[pathNumber][1] = res[1][index].data[pathNumber][1] + obj.number;
                                    },
                                    function () {
                                        res[1][index].data.push([obj.referer, obj.number]);
                                    }
                            );

                        };

                        var add = function (res, obj) {
                            var part = {
                                drilldown: obj.host,
                                name: obj.host,
                                visible: true,
                                y: obj.number
                            };

                            //we don't want a drilldown for direct access.
                            if (obj.host == "direct") {
                                delete part.drilldown;
                            }

                            var drilldown = {id: obj.host, name: obj.host, data: []};
                            drilldown.data.push([obj.referer, obj.number]);
                            res[1].push(drilldown);

                            res[0].push(part);

                        };

                        var fnreduce = function (res, obj) {

                            filtered(res[0], obj.host, exist, add, res, obj);

                            return res;
                        };

                        return data.reduce(fnreduce, [[], []]);

                    },
                    "getPieAffiliate": function (source) {

                        return source.reduce(function (res, obj) {
                            res.push({name: obj.promoter.name, y: obj.orders.length});
                            return res;
                        }, []);
                    },
                    "getPieDiscounts": function (source) {

                        return source.reduce(function (res, obj) {
                            res.push({name: obj.code, y: obj.totalQuantity - obj.remainingQuantity});
                            res[0].y += obj.remainingQuantity;
                            return res;
                        }, [{name: 'Discounts left', y: 0, sliced: true, selected: true}]);
                    },
                    "getChartLinks": function (source) {
                        return source.reduce(function (res, obj) {
                            res[0].push(obj.name);
                            res[1].push(obj.orders);
                            res[2].push(obj.visits);
                            return res;
                        }, [[], [], []]);
                    }
                };
            }]);
