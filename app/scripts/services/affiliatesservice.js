'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.affiliatesservice
 * @description
 * # affiliatesservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('AffiliatesService', ['ngResource'])
        .factory('Affiliates', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/affiliates/:action/:id', {id: '@id'}, {
                    list: {method: 'GET', params: {action: 'getAffiliates'}, isArray: false},
                    add: {method: 'POST', params: {action: 'add'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);
        