'use strict';
angular.module('EventsServices', ['ngResource'])
        .factory('Events', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/events/:action/:offset/:edit', {offset: '@offset', edit:'@edit'}, {
                    manage: {method: 'GET', params: {action: 'manage'}, isArray: false},
                    event: {method: 'GET', params: {action: 'event'}, isArray: false},
                    events: {method: 'GET', params: {action: 'eventsList'}, isArray: true},
                    selling: {method: 'GET', params: {action: 'getSellingEvents'}, isArray: true},
                    getRootsCategories: {method: 'GET', params: {action: 'getRootsCategories'}},
                    getVenues: {method: 'GET', params: {action: 'searchVenue'}},
                    add: {method: 'POST', params: {action: 'add'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    confirm: {method: 'GET', params: {action: 'setConfirmEvent'}},
                    highlight: {method: 'GET', params: {action: 'setHighlightEvent'}},
                    dates: {method: 'GET', params: {action: 'eventGroupedByDate'}},
                    eventbydate: {method: 'GET', params: {action: 'eventsByDate'}, isArray: true},
                    eventsSearch: {method: 'GET', params: {action: 'eventsSearch'}, isArray: true},
                    setPublished: {method: 'POST', params: {action: 'setPublished'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);
