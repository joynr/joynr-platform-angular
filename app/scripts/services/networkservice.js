'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.networkservice
 * @description
 * # networkservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('networkServices', ['ngResource'])
        .service('Network', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/network/:action/:id', {id: '@id'}, {
                    getvisitors: {method: 'GET', params: {action: 'getAllVisitors'}, isArray: true},
                    filter: {method: 'POST', params: {action: 'filter'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}, isArray: true}
                });
            }]);