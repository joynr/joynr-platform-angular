"use strict";
angular.module("LoginInterceptorServices", [])
        .factory("LoginInterceptor", ["$injector", "$location", "$cookies", "$rootScope", "$window", "myConfig", function ($injector, $location, $cookies, $rootScope, $window, myConfig) {
                return {
                    // optional method
                    "request": function (config) {

                        //change with token if one is created
                        if ($cookies.get("user.key") && $cookies.get("user.name")) {
                            config.headers.Authorization = $cookies.get("user.key");
                            $rootScope.name = decodeURIComponent($cookies.get("user.name").replace(/\+/g, ' '));
                            $rootScope.fbid = $cookies.get("user.fbid");
                            $rootScope.admin = $cookies.get("user.admin");
                            $rootScope.userid = $cookies.get("user.id");
                        } else {
                            $window.location = myConfig.login;
                        }

                        return config;
                    },
                    "response": function (response) {

                        if (response.data.logged_in === false) {

                            $window.location = myConfig.login;

                            return response;
                        }

                        return response;

                    },
                    "responseError": function (rejection) {

                        if (rejection.status === 401) {

                            $window.location = myConfig.login;

                            return rejection;
                        }

                        return rejection;

                    }
                };
            }]);