'use strict';

/**
 * @ngdoc service
 * @name SpinnerServices.SpinnerInjector
 * @description
 * # SpinnerServices
 * Service in the joynrPromotersAngularApp.
 */
angular.module('SpinnerServices', [])
        .factory('SpinnerInjector', ['$q', '$rootScope', function ($q, $rootScope) {

                var numLoadings = 0;

                return {
                    request: function (config) {
                        
                        numLoadings++;
                        if (!config.url.match(/^.+events\/event\/\d+/) && !config.url.match(/^.+accounts\/usersSearch\/\w+/)) {
                            // Show loader
                            $rootScope.$broadcast("loader_show");
                        }
                        return config || $q.when(config);

                    },
                    response: function (response) {

                        if ((--numLoadings) === 0) {
                            // Hide loader
                            $rootScope.$broadcast("loader_hide");
                        }

                        return response || $q.when(response);

                    },
                    responseError: function (response) {

                        if (!(--numLoadings)) {
                            // Hide loader
                            $rootScope.$broadcast("loader_hide");
                        }

                        return $q.reject(response);
                    }
                };
            }]);
