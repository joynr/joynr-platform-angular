'use strict';
angular.module('TicketsServices', ['ngResource'])
        .factory('Tickets', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/tickets/:action/:option', {option: '@option'}, {
                    list: {method: 'GET', params: {action: 'getTickets'}, isArray: false},
                    addguest: {method: 'POST', params: {action: 'addGuest'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    addlist: {method: 'POST', params: {action: 'addList'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    checkin: {method: 'POST', params: {action: 'checkin'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    resetcheckin: {method: 'POST', params: {action: 'resetcheckin'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    cancelTicket: {method: 'POST', params: {action: 'cancelTicket'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    refundTicket: {method: 'POST', params: {action: 'refundTicket'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    resendTickets: {method: 'POST', params: {action: 'resendTickets'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    transferTicket: {method: 'POST', params: {action: 'transferTickets'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    suppreslist: {method: 'GET', params: {action: 'suppressTickets'}, isArray: false}
                });
            }]);