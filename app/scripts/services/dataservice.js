'use strict';
/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.dataservice
 * @description
 * # dataservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('dataservice', ["EventsServices", "AffiliatesService", "discountsService", "TicketsServices", "networkServices", "linksService", "angularMoment"])
        .factory("Data", ["Events", "Affiliates", "Discounts", "Tickets", 'Network', 'Links', function (Events, Affiliates, Discounts, Tickets, Network, Links) {
                return {
                    "getEventStats": function (callback) {
                        Events.manage({offset: 0}).$promise.then(callback);
                    },
                    "getEventStat": function (id, callback) {
                        Events.manage({offset: id}).$promise.then(callback);
                    },
                    "getEvents": function (admin, search) {
                        if (admin) {
                            return Events.eventbydate({offset: search});
                        } else {
                            return Events.events({offset: search});
                        }
                    },
                    "getEvent": function (id, callback, edit) {
                        edit = typeof edit !== 'undefined' ? edit : 0;

                        return Events.event({offset: id, edit: edit}).$promise.then(callback);
                    },
                    "getSellingEvents": function (callback) {
                        return Events.selling().$promise.then(callback);
                    },
                    "getOrders": function (source, eventid) {
                        
                        if (eventid.length) {
                            return alasql("SEARCH /orders/WHERE(eventid IN @(?)) FROM ? ", [eventid.join(), source]);
                        }
                        return alasql("SEARCH /orders/ FROM ?", [source]);
                    },
                    "setConfirmEvent": function (id, callback) {
                        Events.confirm({offset: id}).$promise.then(function (result) {
                            if (result.status === 'success') {
                                callback(result.data.confirmed);
                            }
                        });
                    },
                    "setHighlightEvent": function (id, callback) {
                        Events.highlight({offset: id}).$promise.then(function (result) {
                            if (result.status === 'success') {
                                callback(result.data.highlighted);
                            }
                        });
                    },
                    "getDiscounts": function (id, callback) {
                        return Discounts.list({id: id}).$promise.then(callback);
                    },
                    "getLinks": function (id, callback) {
                        return Links.list({id: id}).$promise.then(callback);
                    },
                    "getTicketsList": function (id, callback) {
                        return Tickets.list({option: id}).$promise.then(callback);
                    },
                    "getListTotalTickets": function (source) {
                        return alasql("VALUE OF SELECT SUM(totalQuantity) as total FROM ?", [source]);
                    },
                    "getListTypeTickets": function (source) {
                        return alasql("SELECT DISTINCT(ticketname) as name, totalQuantity, guest, ticketid FROM ?", [source]);
                    },
                    "getTickets": function (source) {
                        return alasql("SEARCH /tickets/ FROM ?", [source]);
                    },
                    "getTicketsFromUser": function (source, user_id) {
                        return alasql("SELECT * FROM ? WHERE user_id = ?" , [source, user_id]);
                    },
                    "getTotalTicketsSold": function (source) {
                        return alasql("VALUE OF SELECT SUM(totalQuantity-remainingQuantity) as total FROM ? ", [this.getTickets(source)]);
                    },
                    "getTotalTickets": function (source) {
                        return alasql("VALUE OF SELECT SUM(totalQuantity) as total FROM ? ", [this.getTickets(source)]);
                    },
                    "getVisitors": function (id, callback) {
                        return Network.getvisitors({id: id}).$promise.then(callback);
                    },
                    "getAffiliates": function (id, callback) {
                        return Affiliates.list({id: id}).$promise.then(callback);
                    },
                    "getLinkAffiliates": function (source) {
                        return alasql("SELECT * FROM ? WHERE link != NULL", [source]);
                    },
                    "getNetwork": function (callback) {
                        return Network.query().$promise.then(callback);
                    },
                    "getNetworkEvents": function (source) {

                        alasql.fn.substring = function (str) {
                            if ( str != null && str.length > 24) {
                                return str.substring(0, 24) + '...';
                            }
                            return str;
                        };

                        var result = alasql("SEARCH DISTINCT(/orders/) FROM ?", [source]);
                        return alasql("SELECT DISTINCT(event) AS complete, eventid AS id, substring(event) AS name FROM ?", [result]).sort(function (a, b) {
                            var textA = a.name ? a.name.toUpperCase() : "";
                            var textB = b.name ? b.name.toUpperCase() : "";
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });
                    },
                    "getGroups": function (source) {
                        var groups = alasql("SEARCH /groups/ FROM ?", [source]);
                        
                        return alasql("SELECT grouplabel, groupid, COUNT(groupid) AS number FROM  ? GROUP BY groupid, grouplabel", [groups]);
                    },
                    "getCategories": function (source) {
                        var result = alasql("SEARCH DISTINCT(/orders/) FROM ?", [source]);
                        
                         return alasql("SELECT DISTINCT(category) AS name, categoryid AS id FROM ?", [result]).sort(function (a, b) {
                            var textA = a.name ? a.name.toUpperCase() : "";
                            var textB = b.name ? b.name.toUpperCase() : "";
                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                        });
                    },
                    "getExcelEmail": function (source) {
                        return alasql('SELECT username AS Name, email AS Email, phone AS Phone, lastevent AS Last_Event, category AS Favorite_Category, attendee AS Events_Attended  INTO XLSX("emails.xlsx",{headers:true}) FROM ?', [source]);
                    },
                    "getTotalRevenue": function (source) {
                        return alasql("VALUE OF SELECT SUM(totalspent) as total FROM ? WHERE totalspent > 0", [source]);
                    },
                    "suppressList": function (id, callback) {
                        return Tickets.suppreslist({option: id}).$promise.then(callback);
                    }
                };
            }]);
