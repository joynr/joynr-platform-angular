'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.discountsservice
 * @description
 * # Linksservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('linksService', ['ngResource'])
        .factory('Links', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/links/:action/:id', {id: "@id"}, {
                    list: {method: 'GET', params: {action: 'getLinks'}, isArray: false},
                    add: {method: 'POST', params: {action: 'add'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);
