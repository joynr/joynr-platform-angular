'use strict';

/**
 * @ngdoc service
 * @name UserService.User
 * @description
 * # userservice
 * Service in the joynrPromotersAngularApp.
 */
angular.module('UserServices', ['ngResource'])
        .service('User', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/account/:action', {} ,{
                    setinfo: {method: 'POST', params: {action: 'setInfo'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    setpassword: {method: 'POST', params: {action: 'setPassword'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
                    setpaypal: {method: 'POST', params: {action: 'setPaypalAddress'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
                });
            }]);