'use strict';

/**
 * @ngdoc service
 * @name joynrPromotersAngularApp.groupsservice
 * @description
 * # groupsservice
 * Service in the joynrPromotersAngularApp.
 */

angular.module('GroupsServices', ['ngResource'])
        .service('Groups', ['myConfig', '$resource',
            function (myConfig, $resource) {
                return $resource(myConfig.backend + '/promoters-api/groups/:action/:param', {param: '@param'}, {
                    add: {method: 'POST', params: {action: 'add'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}, isArray: false},
                    suppress: {method: 'POST', params: {action: 'suppress'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}, isArray: false},
                    manage: {method: 'POST', params: {action: 'modifylist'}, headers: {'Content-Type': 'application/x-www-form-urlencoded'}, isArray: false}
                });
            }]);