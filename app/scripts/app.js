'use strict';

/**
 * @ngdoc overview
 * @name joynrPromotersAngularApp
 * @description
 * # joynrPromotersAngularApp
 *
 * Main module of the application.
 */
angular.module('joynrPromotersAngularApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'EventsServices',
    'TicketsServices',
    'encodeURIComponent',
    'TooltipDirective',
    'angularMoment',
    'ngFileUpload',
    'ngMap',
    'TicketsTableDirective',
    'DateRangeDirective',
    'datetimepicker',
    'MapFormDirective',
    'LoginInterceptorServices',
    'LoaderDirective',
    'SpinnerServices',
    'infinite-scroll',
    'dateBeforeFilter',
    'UserServices',
    'ui.select',
    'dataservice',
    'addGuestformDirective',
    'transferTicketformDirective',
    'stringToNumberDirective',
    'scrollErrorTopDirective',
    'summernote',
    'highcharts-ng',
    'discountsService',
    'DiscountEditableDirective',
    'confirmEventDirective',
    'highlightEventDirective',
    'AccountsServices',
    'DashboardService',
    'AffiliatesTableDirective',
    'AffiliatesService',
    'as.sortable',
    'networkServices',
    'angularUtils.directives.dirPagination',
    'addListFormDirective',
    'linkTableDirective',
    'linksService',
    'jlareau.bowser',
    'GroupsServices',
    'addFilterDirectives',
    'fromListFilter',
    'iframeDirective',
    'color.picker'
])
        .constant('myConfig', {
            links: [{"id": 0, "name": "Iframe"}, {"id": 1, "name": "Facebook"}, {"id": 2, "name": "Email"}, {"id": 3, "name": "Referral"}],
            // backend: 'http://localhost/~YohannM/joynr/public',
            // login: 'http://localhost/~YohannM/joynr/public/website/platform',
            // backend: 'http://52.32.166.91/joynr',
            // login: 'http://52.32.166.91/joynr/website/platform'
            // backend: 'https://www.dev.joynr.co',
            // login: 'https://www.dev.joynr.co/website/platform'
            backend: 'https://joynr.co',
            login: 'https://joynr.co/website/platform'
        })
        .config(function ($routeProvider, $httpProvider) {
            //include the interceptor for login  and loading
            $httpProvider.interceptors.push('LoginInterceptor');
            $httpProvider.interceptors.push('SpinnerInjector');

            //config infinite scroll
            angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);

            $routeProvider
                    .when('/dashboard', {
                        templateUrl: 'views/controller/main.html',
                        controller: 'MainCtrl'
                    })
                    .when('/events', {
                        templateUrl: 'views/controller/events.html',
                        controller: 'EventsCtrl'
                    })
                    .when('/events/:date', {
                        templateUrl: 'views/controller/events.html',
                        controller: 'EventsCtrl'
                    })
                    .when('/review/:id/:success?', {
                        templateUrl: 'views/controller/review.html',
                        controller: 'ReviewCtrl'
                    })
                    .when('/tickets/:id', {
                        templateUrl: 'views/controller/tickets.html',
                        controller: 'TicketsCtrl'
                    })
                    .when('/discounts/:id', {
                        templateUrl: 'views/controller/discounts.html',
                        controller: 'DiscountsCtrl'
                    })
                    .when('/affiliates/:id', {
                        templateUrl: 'views/controller/affiliates.html',
                        controller: 'AffiliatesCtrl'
                    })
                    .when('/edit/:id', {
                        templateUrl: 'views/controller/edit.html',
                        controller: 'EditCtrl'
                    })
                    .when('/event/add/', {
                        templateUrl: 'views/controller/add.html',
                        controller: 'AddCtrl',
                    })
                    .when('/account', {
                        templateUrl: 'views/controller/account.html',
                        controller: 'AccountCtrl'
                    })
                    .when('/help', {
                        templateUrl: 'views/controller/help.html',
                        controller: 'HelpCtrl'
                    })
                    .when('/payements', {
                        templateUrl: 'views/controller/payements.html',
                        controller: 'PayementsCtrl'
                    })
                    .when('/logout', {
                        controller: 'LogoutCtrl',
                        templateUrl: 'views/controller/logout.html'
                    })
                    .when('/accounts', {
                        templateUrl: 'views/controller/accounts.html',
                        controller: 'AccountsCtrl'
                    })
                    .when('/network', {
                        templateUrl: 'views/controller/network.html',
                        controller: 'NetworkCtrl',
                        controllerAs: 'network'
                    })
                    .when('/links/:id', {
                        templateUrl: 'views/controller/links.html',
                        controller: 'LinksCtrl'
                    })
                    .otherwise({
                        redirectTo: '/logout'
                    });
        })
        .run(function ($rootScope, $location) {
            $rootScope.location = $location;
        })
        .value('angularMomentConfig', {
            timezone: 'America/Los_Angeles' // e.g. 'Europe/London'
        });