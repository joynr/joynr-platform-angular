'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:scrollerrortopdirective
 * @description
 * # scrollerrortopdirective
 */
angular.module('scrollErrorTopDirective', [])
        .directive('scrollerrortop', [function () {
                return {
                    restrict: 'A',
                    scope: {
                        form: "="
                    },
                    link: function (scope, elem) {
                        elem.on('submit', function () {

                            // find the first invalid element
                            var firstInvalid = elem[0].querySelector('.ng-invalid');
                           
                            // if we find one, set focus
                            if (firstInvalid) {
                                // we focus on the first input
                                firstInvalid.focus();
                                //we scroll to it
                                jQuery("html,body").animate({ scrollTop: 0});
                            } 
                        });
                    }
                };
            }]);
