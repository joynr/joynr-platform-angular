'use strict';

angular.module('TicketsTableDirective', ["angularMoment", "datetimepicker"]).directive('ticketstable', ["moment", function (moment) {
        return {
            restrict: 'A',
            templateUrl: 'views/directives/tickets-table.html',
            scope: {
                tickets: "=",
                formevent: "=",
                maxdate: "=",
                id: "="
            },
            controller: function ($scope) {

                console.log('go');
                console.log($scope.tickets);
       
                $scope.addTicket = function (price) {
                    $scope.tickets.push({
                        "name": "",
                        "totalQuantity": "",
                        "description": "",
                        "price": price,
                        "dateEnd": moment($scope.maxdate, moment.ISO_8601),
                        "limitPerUser": ""
                    });
                };

                $scope.removeTicket = function (index) {
                    $scope.tickets.splice(index, 1);
                };
                
                $scope.soldoutTicket = function () {
                    this.ticket.totalQuantity = this.ticket.totalQuantity - this.ticket.remainingQuantity;
                };
            }
        };
    }]);
