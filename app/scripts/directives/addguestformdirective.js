'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:addguestform
 * @description
 * # addguestform
 */
angular.module('addGuestformDirective', [])
        .directive('addguestform', [function () {
                return {
                    templateUrl: 'views/directives/addguest-form.html',
                    restrict: 'A',
                    scope: {
                        ticketlist: '=',
                        refresh: '&',
                        submitapi: '&'
                    },
                    controller: function ($scope) {
                        $scope.submit = function () {
                            $scope.submitapi({guest: $scope.guest}).$promise.then(function (result) {
                                if (result.errors) {
                                    $scope.error = true;
                                    $scope.errors = result.errors;
                                } else {
                                    $scope.guest = {};
                                    $scope.form.$setPristine();
                                    $scope.form.$setUntouched();
                                    $scope.success = true;
                                    $scope.refresh();
                                }
                            });
                        };
                    }
                };
            }]);
