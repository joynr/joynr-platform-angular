'use strict';

angular
        .module('datetimepicker', [])

        .provider('datetimepicker', function () {
            var default_options = {};

            this.setOptions = function (options) {
                default_options = options;
            };

            this.$get = function () {
                return {
                    getOptions: function () {
                        return default_options;
                    }
                };
            };
        })

        .directive('datetimepicker', [
            '$timeout',
            'datetimepicker',
            'moment',
            function ($timeout,
                    datetimepicker,
                    moment) {

                var default_options = datetimepicker.getOptions();

                return {
                    require: '?ngModel',
                    restrict: 'AE',
                    scope: {
                        datetimepickerOptions: '@'
                    },
                    link: function ($scope, $element, $attrs, ngModelCtrl) {
                        var passed_in_options = $scope.$eval($attrs.datetimepickerOptions);
                        var options = jQuery.extend({}, default_options, passed_in_options);

                        $element
                                .on('dp.change', function (e) {
                                    if (ngModelCtrl) {
                                        $timeout(function () {
                                            ngModelCtrl.$setViewValue(e.target.value);
                                        });
                                    }
                                })
                                .datetimepicker(options);

                        function setPickerValue() {
                            var date = options.defaultDate || null;

                            if (ngModelCtrl && ngModelCtrl.$modelValue) {
                                date = moment(ngModelCtrl.$modelValue, moment.ISO_8601);
                            }

                            $element
                                    .data('DateTimePicker')
                                    .date(date);
                        }

                        if (ngModelCtrl) {
                            ngModelCtrl.$render = function () {
                                setPickerValue();
                            };
                        }

                        setPickerValue();

                        $scope.$watch(
                                function () {
                                    return ngModelCtrl.$modelValue;
                                },
                                function (newValue, oldValue) {
                                    if (newValue != oldValue) {
                                        var date = moment(ngModelCtrl.$modelValue, "MM-DD-YYYY");
                                        $element.data('DateTimePicker').date(date);
                                    }
                                }, true);
                    }
                };
            }
        ]);