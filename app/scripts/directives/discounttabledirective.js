'use strict';
/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:discounttabledirective
 * @description
 * # discounttabledirective
 */
angular.module('DiscountTableDirective', ["angularMoment", "datetimepicker"])
        .directive('discounttable', ["moment", function (moment) {
                return {
                    restrict: 'A',
                    templateUrl: 'views/directives/discounts-table.html',
                    scope: {
                        discounts: "=",
                        tickets: "=",
                        form: "=",
                        maxdate: "="
                    },
                    controller: function ($scope) {

                        $scope.addDiscount = function (price) {

                            $scope.discounts.push({
                                "name": "",
                                "tickets": {id: '', name: 'All Tickets'},
                                "totalQuantity": "",
                                "description": "",
                                "price": price,
                                "unique": "",
                                "dateEnd": moment($scope.maxdate),
                                "limitPerUser": ""
                            });
                        };
                        $scope.removeDiscount = function (index) {
                            $scope.discounts.splice(index, 1);
                        };
                    }
                };
            }]);