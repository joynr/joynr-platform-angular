'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:stringtonumberdirective
 * @description
 * # stringtonumberdirective
 */
angular.module('stringToNumberDirective', [])
        .directive('stringToNumber',[ function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$formatters.push(function (value) {
                        return parseFloat(value, 10);
                    });
                }
            };
        }]);
