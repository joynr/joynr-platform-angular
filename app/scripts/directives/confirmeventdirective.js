'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:confirmeventdirective
 * @description
 * # confirmeventdirective
 */
angular.module('confirmEventDirective', ['dataservice'])
        .directive('confirmevent', ["Data", function (Data) {
                return {
                    restrict: 'A',
                    templateUrl: 'views/directives/confirm-event.html',
                    scope: {
                        event: '='
                    },
                    controller: function ($scope) {
                        //confirm event
                        $scope.confirm = function () {
                            Data.setConfirmEvent($scope.event.id, function (confirm) {
                                $scope.event.confirmed = confirm;
                            });

                        };
                    }
                };
            }]);
