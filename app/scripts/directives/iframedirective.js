'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:iframedirective
 * @description
 * # iframedirective
 */
angular.module('iframeDirective', [])
        .directive('iframe', function () {
            return {
                restrict: 'A',
                templateUrl: 'views/directives/iframe.html',
                scope: {
                    links: "="
                },
                controller: function ($scope) {
                    var $this = this;
                    
                    $scope.options = {
                        format: 'hex'
  
                    }
                    
                    $scope.iframe = {
                        "id": 0,
                        "height": 650,
                        "width": 600,
                        "backgroundcolor": "#FFFFFF"
                    };

                    $this.linksFilter = function (newValue, oldValue) {
                        if (newValue && newValue !== oldValue ) {
                            var link = $scope.links.filter(function (ele) {
                                return ele.type == 0;
                            });
                            
                            $scope.iframe.id = link[0].id || 0;
                        }
                    };

                    $scope.$watch(function ($scope) {
                        return  $scope.links;
                    }, $this.linksFilter, true);

                    

                }
            };
        });