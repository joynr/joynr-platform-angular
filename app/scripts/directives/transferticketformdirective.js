'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:transferticketform
 * @description
 * # addguestform
 */
angular.module('transferTicketformDirective', [])
        .directive('transferticketform', [function () {
                return {
                    templateUrl: 'views/directives/transferticket-form.html',
                    restrict: 'A',
                    scope: {
                        addnewticketline: '&',
                        resettransferform: '=',
                        submitapi: '&',
                        refresh: '&'
                    },
                    controller: function ($scope) {
                        $scope.submit = function () {
                            $scope.submitapi({transfer: $scope.transfer}).$promise.then(function (result) {
                                if (result.errors) {
                                    $scope.error = true;
                                    $scope.errors = result.errors;
                                } else if (result.success) {
                                    $scope.transfer = {};
                                    $scope.form.$setPristine();
                                    $scope.form.$setUntouched();
                                    $scope.success = true;
                                    $scope.refresh();

                                    $scope.addnewticketline({ticket: result.ticket});
                                }
                            });
                        };

                        $scope.resettransferform = function () {
                            $scope.transfer = {};
                            $scope.form.$setPristine();
                            $scope.form.$setUntouched();
                        };
                       
                        $scope.refreshUsers = function () {
                            if ( typeof $scope.transfer.email != typeof undefined && $scope.transfer.email.length > 6) {
                                return $scope.refresh({search: $scope.transfer.email}).$promise.then(function (results) {
                                    $scope.transfer.name = results.data.name;
                                });
                            }
                        };
                    }
                };
            }]);