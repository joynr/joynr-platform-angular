'use strict';
/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:discounttabledirective
 * @description
 * # discounttabledirective
 */
angular.module('DiscountEditableDirective', ["angularMoment", "datetimepicker"])
		.directive('editable', ["moment", function (moment) {
				return {
					restrict: 'E',
					replace : true,
					templateUrl: 'views/directives/discounts-editable.html',
					scope: {
						discount: "@"
					},
					controller: function ($scope){
						// find the input elemnt of this directive ...
						// var input = element.find('input');
						// // and listen for blur event
						// input.bind('blur', function(){
						// 	// since blur event occured ouside the angular execution context
						// 	// we need to call scope.$apply to tell angularjs about the changes
						// 	scope.$apply(function(){
						// 		// the change is to disable the editMode
						// 		scope.editMode = false;
						// 	});
						// });
					}
  				}
			}
		]
	);
						// $scope.addDiscount = function (price) {

						// 	$scope.discounts.push({
						// 		"name": "",
						// 		"tickets": {id: '', name: 'All Tickets'},
						// 		"totalQuantity": "",
						// 		"description": "",
						// 		"price": price,
						// 		"unique": "",
						// 		"dateEnd": moment($scope.maxdate),
						// 		"limitPerUser": ""
						// 	});
						// };
						// $scope.removeDiscount = function (index) {
						// 	$scope.discounts.splice(index, 1);
						// };
