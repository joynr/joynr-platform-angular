'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:addlistdirective
 * @description
 * # addlistdirective
 */
angular.module('addListFormDirective', [])
        .directive('addlistform', function () {
            return {
                templateUrl: 'views/directives/addlist-form.html',
                restrict: 'A',
                scope: {
                    refresh: '&',
                    submitapi: '&'
                },
                controller: function ($scope) {
                    $scope.submit = function () {
                        $scope.submitapi({list: $scope.list}).$promise.then(function (result) {
                            if (result.errors) {
                                $scope.error = true;
                                $scope.errors = result.errors;
                            } else {
                                $scope.list = {};
                                $scope.form.$setPristine();
                                $scope.form.$setUntouched();
                                $scope.success = true;
                                $scope.refresh();
                            }
                        });
                    };
                }
            };
        });
