'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:highlighteventdirective
 * @description
 * # highlighteventdirective
 */
angular.module('highlightEventDirective', ['dataservice'])
        .directive('highlightevent', ["Data", function (Data) {
                return {
                    restrict: 'A',
                    templateUrl: 'views/directives/highlight-event.html',
                    scope: {
                        event: '='
                    },
                    controller: function ($scope) {
                        //confirm event
                        $scope.highlight = function () {
                            Data.setHighlightEvent($scope.event.id, function (highlight) {
                                $scope.event.highlight = highlight;
                            });

                        };
                    }
                };
            }]);
