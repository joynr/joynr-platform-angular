'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:linktabledirective
 * @description
 * # linktabledirective
 */
angular.module('linkTableDirective', [])
        .directive('linktable', ['myConfig', function (myConfig) {
                return {
                    restrict: 'A',
                    templateUrl: 'views/directives/links-table.html',
                    scope: {
                        links: "=",
                        form: "=",
                        types: "="
                    },
                    controller: function ($scope) {
                        var $this = this;

                        $scope.types = myConfig.links;


                        $scope.types = $scope.types.filter(function (ele) {
                            return ele.id > 0;
                        });
                        
                        $scope.addLinks = function () {
                            $scope.links.push({
                                "name": "",
                                "type": ""
                            });
                        };
                        $scope.removeLink = function (index) {
                            $scope.links.splice(index+1, 1);
                        };
                    }
                };
            }]);
