'use strict';

angular.module('DateRangeDirective', ['angularMoment', 'datetimepicker']).directive('daterange', ["moment", function (moment) {
        return {
            require: '^ngModel',
            restrict: 'A',
            scope: {
                startdate: '=',
                enddate: '='
            },
            templateUrl: 'views/directives/date-range.html',
            controller: function ($scope) {
                var minStartDate = moment().format("YYYY-MM-DD");
                var minEndDate = moment(minStartDate).add(1, "d").format("YYYY-MM-DD");

                $scope.start_date = moment($scope.startdate);
                $scope.start_hour = moment($scope.start_date).format("LT");
                $scope.enddateevent = moment($scope.enddate);
                $scope.endhourevent = moment($scope.enddateevent).format("LT");
                
                // watch scope because we are in asynchrone
                $scope.$watch(
                        function () {
                            return $scope.startdate;
                        },
                        function (newValue, oldValue) {
                            if (newValue && !oldValue) {
                                // add one day because we have a bug that minus one day on all date
                                $scope.start_date = moment($scope.startdate);
                                $scope.start_hour = moment($scope.start_date).format("LT");
                            }
                        }, true);

                $scope.$watch(
                        function () {
                            return $scope.enddate;
                        },
                        function (newValue, oldValue) {
                            if (newValue && !oldValue) {
                                // add one day because we have a bug that minus one day on all date
                                $scope.enddateevent = moment($scope.enddate);
                                $scope.endhourevent = moment($scope.enddateevent).format("LT");
                            }
                        }, true);

                $scope.datetimepicker_options_start = {
                    minDate: minStartDate,
                    format: "MM-DD-YYYY"
                };

                $scope.datetimepicker_options_end = {
                    minDate: minEndDate,
                    format: "MM-DD-YYYY"
                };

                //init and refresh date
                $scope.getDateStartAndEnd = function (hour) {
                   
                    //stop changep propagation when we click on one date
                    if (!$scope.clickeventdate && !hour) {
                        return;
                    }

                    $scope.clickeventdate = false;

                    // create the date var
                    var dateStart = moment($scope.start_date + " " + $scope.start_hour, "MM-DD-YYYY h:mm A");
                    var dateEnd = moment($scope.enddateevent + " " + $scope.endhourevent, "MM-DD-YYYY h:mm A");
                    
                    //validate range
                    if (dateStart.isBefore(moment())) {
                        dateStart = moment().add(1, "d").hour(22).minute(0).second(0);
                    }

                    if (dateStart.isAfter(dateEnd)) {
                        dateEnd = moment(dateStart).hour(2).minute(0).second(0).add(1, 'days');
                    }

                    $scope.start_date = moment(dateStart).format("MM-DD-YYYY");
                    $scope.start_hour = moment(dateStart).format("LT");
                    $scope.enddateevent = moment(dateEnd).format("MM-DD-YYYY");
                    $scope.endhourevent = moment(dateEnd).format("LT");

                    //add to the post object
                    $scope.startdate = dateStart.format("YYYY-MM-DD\THH:mm:ss");
                    $scope.enddate = dateEnd.format("YYYY-MM-DD\THH:mm:ss");
                };
            }
        };
    }]);
