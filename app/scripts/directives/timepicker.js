'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:timepicker
 * @description
 * # timepicker
 */
angular.module('joynrPromotersAngularApp')
        .directive('timepicker', function () {
            return {
                restrict: 'AE',
                scope: {
                    defaultTime: '='
                },
                link: function ($scope, $element) {
                    $element.timepicker({
                        defaultTime: $scope.defaultTime
                    });
                }
            };
        });
