'use strict';

angular.module('TooltipDirective', []).directive('tooltip', ["$rootScope", function ($rootScope) {
        return {
            restrict: 'A',
            link: function (scope, element) {
                jQuery(element).hover(function () {
                    // on mouseenter
                    jQuery(element).tooltip('show');

                }, function () {
                    // on mouseleave
                    jQuery(element).tooltip('hide');
                });

                $rootScope.$on('$routeChangeSuccess', function () {
                    jQuery('[tooltip]').each(function () {
                        jQuery(this).tooltip('hide');
                    });
                });

            }
        };
    }]);
