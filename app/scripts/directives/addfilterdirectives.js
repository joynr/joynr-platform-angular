'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:addfilterdirectives
 * @description
 * # addfilterdirectives
 */
angular.module('addFilterDirectives', ['ui.select'])
        .directive('addfilter', [function () {
                return {
                    templateUrl: 'views/directives/addfilter.html',
                    restrict: 'A',
                    scope: {
                        'filterslist': "=",
                        'filters': "="
                    },
                    controller: function ($scope) {
                        var $this = this;

                        $scope.filters = [{}];

                        $scope.addFilter = function () {
                            $scope.filters.push({});
                        };

                        $scope.removeFilter = function (index) {
                            $scope.filters.splice(index, 1);
                        };

                        $this.fnFiltersList = function (newValue, oldValue) {
                            if (newValue && !oldValue) {
                                $scope.filterslist = newValue;
                            }
                        };

                        // get the segments
                        $scope.$watch(function () {
                            return $scope.filterslist;
                        }, $this.fnFiltersList, true);
                    }
                };
            }]);
