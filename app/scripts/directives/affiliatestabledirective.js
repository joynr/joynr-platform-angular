'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:affiliatestabledirective
 * @description
 * # affiliatestabledirective
 */
angular.module('AffiliatesTableDirective', [])
        .directive('affiliatestable', [function () {
                return {
                    restrict: 'A',
                    templateUrl: 'views/directives/affilitates-table.html',
                    scope: {
                        affiliates: "=",
                        form: "=",
                        refresh: "&"
                    },
                    controller: function ($scope) {
                        $scope.users = [];
                       
                        $scope.refreshUsers = function (search) {
                            console.log(search);
                            if (search.length > 7) {
                                return $scope.refresh({search: search}).$promise.then(function (results) {
                                    $scope.users = results;
                                });
                            }
                        };

                        $scope.addAffiliate = function () {
                            $scope.affiliates.push({});
                        };

                        $scope.removeAffiliate = function (index) {
                            $scope.affiliates.splice(index, 1);
                        };

                        $scope.checkAll = function (affiliate) {
                            affiliate.roles.manage = affiliate.roles.admin;
                            affiliate.roles.tickets = affiliate.roles.admin;
                            affiliate.roles.discounts = affiliate.roles.admin;
                            affiliate.roles.guest = affiliate.roles.admin;
                            affiliate.roles.edit = affiliate.roles.admin;
                            affiliate.roles.link = affiliate.roles.admin;
                        };
                    }
                };
            }]);
