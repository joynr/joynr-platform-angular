'use strict';

angular.module('MapFormDirective', ['ngMap']).directive('mapform', ['$filter', 'NgMap', 'GeoCoder', function ($filter, NgMap, GeoCoder) {
        return {
            restrict: 'A',
            scope: {
                event: "=",
                markers: "=",
                formevent: '=',
                collapse: "=",
                help: "="
            },
            templateUrl: 'views/directives/map-form.html',
            link: function ($scope) {
                var explacename;

                NgMap.getMap().then(function (map) {
                    $scope.map = map;
                });
                // get the markers
                $scope.$watch(
                        function () {
                            return $scope.markers;
                        },
                        function (newValue, oldValue) {
                            //if old value is undefined and new defined we fire the getSubCat function for editing value
                            if (newValue && !oldValue) {
                                $scope.markers = newValue;
                            }
                        }, true);

                //test if we have a place

                $scope.$watch(
                        function () {
                            return $scope.event;
                        },
                        function (newValue) {
                            //if old value is undefined and new defined we fire the getSubCat function for editing value
                            if (newValue && newValue.venueHiddenSelect > 0) {
                                $scope.address = newValue.placeName + ", " + newValue.placeAddress + ", " + newValue.placePostalCode + " " + newValue.placeCity;
                                $scope.addVenue = true;
                            }
                        }, true);

                $scope.infos = {};
                $scope.choose = {};
                $scope.place = {};

                var placeChanged = function (placeChoose) {

                    //if we have a new place we retrive the data and add it to the form
                    if (placeChoose.address_components) {
                        $scope.event.venueHiddenSelect = "";
                        $scope.event.placeName = "";
                        $scope.event.placeType = "";
                        $scope.event.placeAddress = "";
                        $scope.event.placePostalCode = "";
                        $scope.event.placeCity = "";
                        $scope.event.placeURL = "";
                        $scope.event.placeLat = "";
                        $scope.event.placeLng = "";
                        $scope.event.placeDesc = "";

                        $scope.map.markers.search.setVisible(false);

                        var placeCity = $filter('filter')(placeChoose.address_components, {$: "locality"}, true);
                        var placePostalCode = $filter('filter')(placeChoose.address_components, {$: "postal_code"}, true);
                        var placeAddress = $filter('filter')(placeChoose.address_components, {$: "route"}, true);
                        var placeNumber = $filter('filter')(placeChoose.address_components, {$: "street_number"}, true);

                        //we verify if we have not the venur in database
                        var ln = parseFloat(placeChoose.geometry.location.lng()).toFixed(6);
                        var la = parseFloat(placeChoose.geometry.location.lat()).toFixed(6);
                        var placeExist = $scope.markers.results.filter(
                                function (obj) {
                                    return ln === parseFloat(obj.lng).toFixed(6) && la === parseFloat(obj.lat).toFixed(6);
                                });
                        if (placeExist.length > 0) {
                            $scope.event.venueHiddenSelect = placeExist[0].id;
                        } else {
                            $scope.event.venueHiddenSelect = 0;
                        }

                        $scope.event.placeName = placeChoose.name || explacename;// we do tah t because when we enter a place with the input we have no name in the result
                        $scope.event.placeType = "";
                        $scope.event.placeAddress = placeAddress.length > 0 ? placeNumber[0].long_name + ' ' + placeAddress[0].long_name : null;
                        $scope.event.placePostalCode = placePostalCode.length > 0 ? placePostalCode[0].long_name : null;
                        $scope.event.placeCity = placeCity.length > 0 ? placeCity[0].long_name : null;
                        $scope.event.placeURL = placeChoose.website;
                        $scope.event.placeLat = placeChoose.geometry.location.lat();
                        $scope.event.placeLng = placeChoose.geometry.location.lng();
                        $scope.event.placeDesc = "";

                        $scope.map.markers.search.setVisible(true);
                        $scope.map.setCenter(placeChoose.geometry.location);

                        if (!$scope.event.placeName || !$scope.event.placeAddress || !$scope.event.placePostalCode || !$scope.event.placeCity) {
                            return $scope.formevent.uPlace.$setValidity('invalid', false);
                        }

                        return $scope.formevent.uPlace.$setValidity('invalid', true);

                    }

                    //else we look at the form if it had good data. 
                    if ($scope.event.placeName && $scope.event.placeAddress && $scope.event.placePostalCode && $scope.event.placeCity) {
                        $scope.address = $scope.event.placeName + ", " + $scope.event.placeAddress + ", " + $scope.event.placePostalCode + " " + $scope.event.placeCity;
                        return $scope.formevent.uPlace.$setValidity('invalid', true);
                    }

                    //If not we return invalid
                    return $scope.formevent.uPlace.$setValidity('invalid', false);
                };

                //button add venue
                $scope.addvenue = false;
                
                $scope.addvenueAction = function(){
                    $scope.addvenue = true;
                    $scope.formevent.uPlace.$setValidity('invalid', true);
                };

                //if we use the autocomplete input
                $scope.placeSearchInputChanged = function () {
                    if (typeof this.getPlace === "function") {
                        $scope.addVenue = true;
                        console.log(this.getPlace());
                        placeChanged(this.getPlace());
                    }
                };

                //hack for the map to resize when showing
                $scope.$watch(
                        function () {
                            return $scope.collapse;
                        },
                        function (newValue) {

                            //we should wait for the panel to appear
                            if (newValue) {
                                setTimeout(function () {
                                    var center = $scope.map.getCenter();
                                    google.maps.event.trigger($scope.map, 'resize');
                                    $scope.map.setCenter(center);
                                }, 50);
                            }
                        }, true);
            }
        };
    }]);
