'use strict';

/**
 * @ngdoc directive
 * @name joynrPromotersAngularApp.directive:focus
 * @description
 * # focus
 */
angular.module('joynrPromotersAngularApp')
        .directive('focus', [function () {
                return {
                    restrict: 'AE',
                    link: function postLink(scope, element) {
                        $(element).on('click', function () {
                            var $this = this;

                            setTimeout(function () {
                                $this.select();
                            }, 1);
                        });
                    }
                };
            }]);
