'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:linktypefilter
 * @function
 * @description
 * # linktypefilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
        .filter('linktype', ['myConfig', function (myConfig) {
                return function (input) {

                    if (input || input === 0) {
                        return myConfig.links.filter(function (obj) {
                            return obj.id == input
                        }).shift().name;
                    }

                    return false;
                };
            }]);
