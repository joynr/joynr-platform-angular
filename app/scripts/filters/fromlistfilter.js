'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:fromListFilter
 * @function
 * @description
 * # fromListFilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('fromListFilter', [])
        .filter('fromList', [function () {
                return function (input, list) {

                    var fnFilter = function (el) {
                        if (!list.length) {
                            return true;
                        }
                        return  !!~list.indexOf(el.id);
                    };

                    return input.filter(fnFilter);
                };
            }]);
