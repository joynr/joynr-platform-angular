'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:dateBeforeFilter
 * @function
 * @description
 * # dateBeforeRepeat
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('dateBeforeFilter')
        .filter('dateBeforeRepeat', ['moment', function (moment) {
                return function (values, date) {
                    switch (date) {
                        case 1:
                            return values.filter(function (value) {
                                return moment(value.dateEnd).isBefore();
                            });
                        case 2:
                            return values.filter(function (value) {
                                return moment(value.dateEnd).isAfter();
                            });
                        default:
                            return values;
                    }
                };
            }]);
