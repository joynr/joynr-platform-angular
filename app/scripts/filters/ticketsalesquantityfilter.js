'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:ticketsalesquantityfilter
 * @function
 * @description
 * # ticketsalesquantityfilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
  .filter('ticketsalesquantity', function () {
    return function (input) {
      return input.reduce(function (res, obj) {
                    res += obj.quantity;
                    return res;
                }, 0);
    };
  });
