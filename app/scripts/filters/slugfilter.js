'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:slugfilter
 * @function
 * @description
 * # slugfilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
        .filter('slugfilter', function () {
            return function (input) {
                return input.toString().toLowerCase()
                        .replace(/\s+/g, '-')           // Replace spaces with -
                        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                        .replace(/^-+/, '')             // Trim - from start of text
                        .replace(/-+$/, '');            // Trim - from end of text
            };
        });
