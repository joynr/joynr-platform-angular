'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:ticketsfilter
 * @function
 * @description
 * # ticketsfilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
        .filter('ticketsfilter', ["Data", function (Data) {
                return function (input, totalticket) {
                    
                    var sale = Data.getTotalTicketsSold([input]);
                    var total = Data.getTotalTickets([input]);
                    
                    if (input)
                    {
                        input.sales = sale;
                    }
                    
                    var result = sale + "/<span class='small-size-text'>" + total + "</span>";
                    
                    if (totalticket) {
                        result = sale;
                    }
                    return result;
                };
            }]);
