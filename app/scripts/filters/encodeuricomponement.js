'use strict';

angular.module('encodeURIComponent', []).filter('encodeURIComponentFilter', function() {
  return function(input) {
    return window.encodeURIComponent(input);
  };
});

