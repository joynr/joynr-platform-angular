'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:dateBeforeFilter
 * @function
 * @description
 * # dateBeforeClassFilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('dateBeforeFilter', ['angularMoment'])
        .filter('dateBeforeClass', ['moment', function (moment) {
                return function (date) {
                        return moment(date).isBefore();
                    };
            }]);
