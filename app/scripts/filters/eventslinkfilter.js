'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:eventslinkfilter
 * @function
 * @description
 * # eventslinkfilter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
        .filter('eventslink', function () {
            return function (ev) {
                if (ev.roles.indexOf('tickets') != -1 && ev.roles.indexOf('admin') == -1  && ev.roles.indexOf('manage') == -1 && ev.roles.indexOf('edit') == -1) {
                    return "tickets/" + ev.id;
                } else if (ev.roles.indexOf('edit') != -1 && ev.roles.indexOf('admin') == -1  && ev.roles.indexOf('manage') == -1 && ev.roles.indexOf('tickets') == -1) {
                    return "edit/" + ev.id;
                }
                return "review/" + ev.id;
            };
        });
