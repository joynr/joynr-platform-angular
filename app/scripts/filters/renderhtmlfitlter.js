'use strict';

/**
 * @ngdoc filter
 * @name joynrPromotersAngularApp.filter:renderhtmlfitlter
 * @function
 * @description
 * # renderhtmlfitlter
 * Filter in the joynrPromotersAngularApp.
 */
angular.module('joynrPromotersAngularApp')
  .filter('renderhtml',["$sce", function ($sce) {
    return function (html_code) {
      return $sce.trustAsHtml(html_code);
    };
  }]);
