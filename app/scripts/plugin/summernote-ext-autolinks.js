(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {
    'use strict';

    var myAutoLink = function (context) {
        var self = this;
        var defaultScheme = 'http://';
        var linkPattern = /^(https?:\/\/|[A-Z0-9._%+-]+@)?(www\.)?(.+)$/i;
        var emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

        this.events = {
            'summernote.keyup': function (we, e) {
                if (!e.isDefaultPrevented()) {
                    self.handleKeyup(e);
                }
            },
            'summernote.keydown': function (we, e) {
                self.handleKeydown(e);
            }
        };

        this.initialize = function () {
            this.lastWordRange = null;
        };

        this.destroy = function () {
            this.lastWordRange = null;
        };

        this.replace = function () {
            if (!this.lastWordRange) {
                return;
            }

            var keyword = this.lastWordRange.toString();
            var match = keyword.match(linkPattern);
            var matchEmail = keyword.match(emailPattern);

            
            if (matchEmail && (matchEmail[1] || matchEmail[2])) {
                console.log(matchEmail);
                var link = 'mailto:' + matchEmail[0] + '?Subject=Question%20concerning%20your%20event'
                var node = $('<a />').html(keyword).attr('href', link)[0];

                this.lastWordRange.insertNode(node);
                this.lastWordRange = null;
                context.invoke('editor.focus');
            } else if (match && (match[1] || match[2])) {
                var link = match[1] ? keyword : defaultScheme + keyword;
                var node = $('<a />').html(keyword).attr('href', link)[0];

                this.lastWordRange.insertNode(node);
                this.lastWordRange = null;
                context.invoke('editor.focus');
            }

        };
     
        this.handleKeydown = function (e) {
            if (e.keyCode == 13 || e.keyCode == 32) {
                var wordRange = context.invoke('editor.createRange').getWordRange();
                this.lastWordRange = wordRange;
            }
        };

        this.handleKeyup = function (e) {
            if (e.keyCode == 13 || e.keyCode == 32) {
                this.replace();
            }
        };
    };

    $.summernote['options']['modules']['autoLink'] = myAutoLink;

}));