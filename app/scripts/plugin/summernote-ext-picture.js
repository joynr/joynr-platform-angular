(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {
'use strict';
    // Extends plugins for adding hello.
    //  - plugin is external module for customizing.
    $.extend($.summernote.plugins, {
        'modal': function (context) {

            var self = this;
            var ui = $.summernote.ui;

            var $editor = context.layoutInfo.editor;
            var options = context.options;
            var lang = options.langInfo;

            context.memo('button.myimagedialog', function () {
                var button = ui.button({
                    contents: '<i class="fa fa-picture-o"/>',
                    tooltip: 'Picture',
                    click: function () {
                        // call bootstrap method
                        self.show();
                    }
                });

                // create jQuery object from button instance.
                var $hello = button.render();
                return $hello;
            });

            this.initialize = function () {
                var $container = options.dialogsInBody ? $(document.body) : $editor;

                var body = '<div class="form-group" style="overflow:auto;">' +
                        '<label>' + lang.image.url + '</label>' +
                        '<input class="note-image-url form-control col-md-12" type="text" />' +
                        '</div>';
                var footer = '<button href="#" class="btn btn-primary note-image-btn disabled" disabled>' + lang.image.insert + '</button>';

                this.$dialog = ui.dialog({
                    title: lang.image.insert,
                    body: body,
                    footer: footer
                }).render().appendTo($container);
            };

            this.destroy = function () {
                ui.hideDialog(this.$dialog);
                this.$dialog.remove();
            };

            this.bindEnterKey = function ($input, $btn) {
                $input.on('keypress', function (event) {
                    if (event.keyCode === key.code.ENTER) {
                        $btn.trigger('click');
                    }
                });
            };

            this.show = function () {
                context.invoke('editor.saveRange');
                this.showImageDialog().then(function (data) {
                    // [workaround] hide dialog before restore range for IE range focus
                    ui.hideDialog(self.$dialog);
                    context.invoke('editor.restoreRange');

                    if (typeof data === 'string') { // image url
                        context.invoke('editor.insertImage', data);
                    } else { // array of files
                        context.invoke('editor.insertImagesOrCallback', data);
                    }
                }).fail(function () {
                    context.invoke('editor.restoreRange');
                });
            };

            /**
             * show image dialog
             *
             * @param {jQuery} $dialog
             * @return {Promise}
             */
            this.showImageDialog = function () {
                return $.Deferred(function (deferred) {
                    var $imageInput = self.$dialog.find('.note-image-input'),
                            $imageUrl = self.$dialog.find('.note-image-url'),
                            $imageBtn = self.$dialog.find('.note-image-btn');

                    ui.onDialogShown(self.$dialog, function () {
                        context.triggerEvent('dialog.shown');

                        // Cloning imageInput to clear element.
                        $imageInput.replaceWith($imageInput.clone()
                                .on('change', function () {
                                    deferred.resolve(this.files || this.value);
                                })
                                .val('')
                                );

                        $imageBtn.click(function (event) {
                            event.preventDefault();

                            deferred.resolve($imageUrl.val());
                        });

                        $imageUrl.on('keyup paste', function () {
                            var url = $imageUrl.val();
                            ui.toggleBtn($imageBtn, url);
                        }).val('').trigger('focus');
                        self.bindEnterKey($imageUrl, $imageBtn);
                    });

                    ui.onDialogHidden(self.$dialog, function () {
                        $imageInput.off('change');
                        $imageUrl.off('keyup paste keypress');
                        $imageBtn.off('click');

                        if (deferred.state() === 'pending') {
                            deferred.reject();
                        }
                    });

                    ui.showDialog(self.$dialog);
                });
            };

        }
    });
}));
