'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:EditCtrl
 * @description
 * # EditCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('EditCtrl', ["$rootScope", "$scope", 'Data', 'Events', '$routeParams', 'moment', 'myConfig', 'Upload', '$filter', '$location', '$route', function ($rootScope, $scope, Data, Events, $routeParams, moment, myConfig, Upload, $filter, $location, $route) {
                //menu general left highlight
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;

                // menu page hightlight
                $scope.tab = "edit";
                var id = $routeParams.id;
                //init var
                $scope.url = myConfig.backend;
                $scope.event = {};

                var init = function (result) {
                    $scope.ev = result;
                    $rootScope.roles = result.roles;
                    //redirect if the events is finisher or started
                    // if ($filter('dateBeforeClass')($scope.ev.dateStart)) {
                    //     $location.path("review/" + id);
                    // }

                    //get all ticket without the guest list
                    var filterGuestTicket = function (obj) {
                        return !obj.guest;
                    };

                    var tickets = $scope.ev.tickets.filter(filterGuestTicket);

                    //create separate object ot not modify the one we show
                    $scope.event = {
                        id: $scope.ev.id,
                        freetickets: $scope.ev.freeTickets,
                        donation: $scope.ev.donation,
                        notifications: $scope.ev.notifications,
                        name: $scope.ev.name,
                        selectEventCat: $scope.ev.categories[0],
                        url: $scope.ev.link === null ? "" : $scope.ev.link,
                        link: $scope.ev.booking_link === null ? "" : $scope.ev.booking_link,
                        social: $scope.ev.social_media_link,
                        startDate: $scope.ev.dateStart, //divide
                        endDate: $scope.ev.dateEnd, //divide
                        desc: $scope.ev.description,
                        venueHiddenSelect: $scope.ev.place.id, //see use
                        placeName: $scope.ev.place.name,
                        placeType: "",
                        placeAddress: $scope.ev.place.address,
                        placePostalCode: $scope.ev.place.postalCode,
                        placeCity: $scope.ev.place.city,
                        placeURL: $scope.ev.place.link,
                        placeLat: $scope.ev.place.latitude,
                        placeLng: $scope.ev.place.longitude,
                        placeDesc: $scope.ev.place.description,
                        tickets: tickets,
                        transferable: $scope.ev.transferable
                    };
                    $scope.picture = $scope.ev.picture === null ? "" : $scope.ev.picture; //if we don't do that it's make some problem due of nested object;
                };

                Data.getEvent(id, init, 1);

                //get categories and sub categories
                $scope.categories = Events.getRootsCategories();
                //map
                $scope.markers = Events.getVenues();
                //submit
                $scope.submit = function () {

                    //format tickets
                    var tickets = $scope.event.tickets.map(function (obj) {
                        var newObj = {};
                        newObj.id = obj.id;
                        newObj.name = obj.name;
                        newObj.quantity = obj.totalQuantity;
                        newObj.price = obj.price;
                        newObj.datetime = moment(obj.dateEnd, 'YYYY-MM-DD LT').format("YYYY-MM-DD\THH:mm:ss");
                        newObj.desc = obj.description ? obj.description : "";
                        newObj.limit = obj.limitPerUser ? obj.limitPerUser : "";
                        newObj.promoterFees = obj.promoterFees ? obj.promoterFees : 0;
                        newObj.hidden = obj.hidden;
                        return newObj;
                    });
                    if ($scope.ev.picture === $scope.picture) {
                        $scope.event.picture = null;
                    }

                    Upload.upload({
                        url: myConfig.backend + '/promoters-api/events/add',
                        method: 'POST',
                        file: $scope.picture,
                        sendFieldsAs: 'form',
                        fields: {
                            id: $scope.event.id,
                            freeTickets: $scope.event.freetickets,
                            donation: $scope.event.donation,
                            notifications: $scope.event.notifications,
                            tickets: tickets,
                            desc: $scope.event.desc,
                            name: $scope.event.name,
                            placeAddress: $scope.event.placeAddress,
                            placeCity: $scope.event.placeCity,
                            placeDesc: $scope.event.placeDesc,
                            placeLat: $scope.event.placeLat,
                            placeLng: $scope.event.placeLng,
                            placeName: $scope.event.placeName,
                            placePostalCode: $scope.event.placePostalCode,
                            placeType: $scope.event.placeType,
                            placeURL: $scope.event.placeURL,
                            selectEventCat: $scope.event.selectEventCat.id,
                            endDate: moment.isMoment($scope.event.endDate) ? $scope.event.endDate.format("YYYY-MM-DD\THH:mm:ss") : $scope.event.endDate,
                            startDate: moment.isMoment($scope.event.startDate) ? $scope.event.startDate.format("YYYY-MM-DD\THH:mm:ss") : $scope.event.startDate,
                            venueHiddenSelect: $scope.event.venueHiddenSelect,
                            url: $scope.event.url,
                            link: $scope.event.link,
                            transferable: $scope.event.transferable
                        }
                    }).then(function (data) {
                        if (data.data.errors) {
                            $scope.senderror = true;
                            $scope.errors = data.data.errors;
                        } else {
                            //we reload the route to return the edit
                            var link = $filter('eventslink')($scope.ev);
                            if (link !== "edit/" + $scope.ev.id) {
                                $location.path(link);
                            }
                            $route.reload();

                        }
                    }, function (data) {
                        // handle error
                        $scope.senderror = true;
                        $scope.errors = data;
                    });
                };

                $scope.name = function (groups) {
                    return groups.map(function (group) {
                        group.items.sort(function (a, b) {
                            return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
                        });
                        return group;
                    });
                };

            }]);
