'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('ReviewCtrl', ['$rootScope', '$scope', 'Data', 'Events', '$routeParams', '$httpParamSerializer', 'Dashboard', 'fakeData', 'myConfig', function ($rootScope, $scope, Data, Events, $routeParams, $httpParamSerializer, Dashboard, fakeData, myConfig) {
                var $this = this;
                //menu general left highlight
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;

                //tabs views
                $scope.tab = "manage";

                //init var
                var id = $routeParams.id;
                $scope.fake = id == 0 ? true : false;
                $scope.url = myConfig.backend;

                $scope.success = $routeParams.success;

                $this.eventCallback = function (result) {
                    $scope.ev = result;

                    $rootScope.roles = result.roles;

                    if ( $scope.fake ) {
                        $this.dashboardCallback(fakeData.dashboard);
                        $this.originCallback(fakeData.visitors);
                    }else if( ($rootScope.roles.indexOf('admin') > -1 || $rootScope.roles.indexOf('manage') > -1) ) {
                        Data.getEventStat(id, $this.dashboardCallback);
                        Data.getVisitors(id, $this.originCallback);
                    }

                    //TICKETS OVERVIEW portlet
                    $scope.TicketsTypeChart = Dashboard.getTicketsTypeTableau([$scope.ev]);
                    $scope.TicketsType = Data.getTickets([$scope.ev]);
                    $scope.tickettotal = Data.getTotalTickets([$scope.ev]);

                    //chart
                    $scope.ticketpiechart = {
                        title: {
                            text: 'Tickets'
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y:.1f}%'
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                        },
                        options: {
                            chart: {
                                type: 'pie'
                            },
                            legend: {
                                align: 'right',
                                x: -70,
                                verticalAlign: 'top',
                                y: 20,
                                floating: true,
                                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                borderColor: '#CCC',
                                borderWidth: 1,
                                shadow: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: true,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black, 0 0 3px black'
                                        }
                                    }
                                }
                            }
                        },
                        series: [{
                                name: 'Ticket sales',
                                colorByPoint: true,
                                data: $scope.TicketsTypeChart
                            }]
                    };

                };

                $this.dashboardCallback = function (data) {

                    $scope.profits = data.profits;
                    $scope.discounts = data.discounts;
                    $scope.donations = data.donations;

                    var orderNumber = data.orders.reduce(function (a, b) {
                        return a + b;
                    });
                    var visitorsNumber = data.visitors.reduce(function (a, b) {
                        return a + b;
                    });
                    
                    $scope.checked = data.checked;
                    $scope.rate = visitorsNumber !== 0 && orderNumber !== 0 ? Math.round((orderNumber / visitorsNumber) * 100) : 0;

                    //Dashboard
                    $scope.dashboardconfig = {
                        "options": {
                            "chart": {
                                "type": "line"
                            },
                            "plotOptions": {
                                "series": {
                                    marker: {
                                        fillColor: '#FFFFFF',
                                        lineWidth: 2,
                                        lineColor: null,
                                        symbol: 'circle'
                                    }
                                }
                            },
                            legend: {
                                align: 'right',
                                verticalAlign: 'top',
                                x: 0
                            },
                            xAxis: {
                                gridLineWidth: 1,
                                title: {
                                    text: null
                                },
                                tickWidth: 0,
                                categories: data.date
                            },
                            yAxis: {
                                gridLineWidth: 0,
                                labels: {
                                    enabled: false
                                },
                                title: {
                                    text: null
                                }
                            },
                        },
                        "series": [
                            {
                                "name": "Visitors",
                                "data": data.visitors,
                                "connectNulls": false,
                                "id": "visitors",
                                "type": "column",
                                "color": "#5e7280"
                            },
                            {
                                "name": "Orders",
                                "data": data.orders,
                                "type": "spline",
                                "id": "orders",
                                "connectNulls": true,
                                "color": "#609cce"
                            },
                            {
                                "name": "Tickets",
                                "data": data.tickets,
                                "type": "spline",
                                "id": "tickets",
                                "connectNulls": true,
                                "color": "#91ccff"
                            }
                        ],
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {
                            height: 475
                        }
                    };
                };

                //network portlet chart 
                this.originCallback = function (result) {

                    $scope.network = Dashboard.getPieOriginFromVisitors(result);

                    $scope.originpiechart = {
                        title: {
                            text: 'Origins'
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.name}: {point.y:.1f}%'
                                }
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                        },
                        options: {
                            chart: {
                                type: 'pie'
                            },
                            drilldown: {
                                series: $scope.network[1]
                            },
                            legend: {
                                align: 'right',
                                x: -70,
                                verticalAlign: 'top',
                                y: 20,
                                floating: true,
                                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                                borderColor: '#CCC',
                                borderWidth: 1,
                                shadow: false
                            },
                            tooltip: {
                                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                            },
                            plotOptions: {
                                column: {
                                    stacking: 'normal',
                                    dataLabels: {
                                        enabled: true,
                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                        style: {
                                            textShadow: '0 0 3px black, 0 0 3px black'
                                        }
                                    }
                                }
                            }
                        },
                        series: [{
                                name: 'Visitors Origin',
                                colorByPoint: true,
                                data: $scope.network[0]
                            }]
                    };
                };

                // Make an event live!
                $scope.publish = function (id) {
                    Events.setPublished($httpParamSerializer({id: id, status: 1}),
                            function ( result )
                            {
                                if ( result.success )
                                {
                                    $scope.ev.published = 1;
                                }
                            });
                };

                //network portlet chart 
                $scope.unpublish = function (id) {
                    Events.setPublished($httpParamSerializer({id: id, status: 0}),
                            function ( result )
                            {
                                if ( result.success )
                                {
                                    $scope.ev.published = 0;
                                }
                            });
                };

                if (!$scope.fake) {
                    Data.getEvent(id, $this.eventCallback);
                } else {
                    $this.eventCallback(fakeData.event);
                }

            }]);
