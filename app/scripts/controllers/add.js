'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AddCtrl
 * @description
 * # AddCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('AddCtrl', ['$rootScope', '$scope', 'Events', 'moment', "Upload", "myConfig", "Data", "$location", function ($rootScope, $scope, Events, moment, Upload, myConfig, Data, $location) {
                //menu variable
                $rootScope.dashboard = false;
                $rootScope.events = false;
                $rootScope.add = true;
                $rootScope.network = false;

                $scope.create = true;

                // INIT
                //get categories
                $scope.categories = Events.getRootsCategories();

                //map
                $scope.markers = Events.getVenues();

                // name
                $scope.name = function (groups) {
                    return groups.map(function (group) {
                        group.items.sort(function (a, b) {
                            return (a.name < b.name) ? -1 : (a.name > b.name) ? 1 : 0;
                        });
                        return group;
                    });
                };

                $scope.event = {
                    startDate: moment().add(1, "d").hour(22).minute(0).second(0).toDate().toISOString(),
                    endDate: moment().add(2, "d").hour(2).minute(0).second(0).toDate().toISOString(),
                    tickets: [],
                    placeLat: 37.7576171,
                    placeLng: -122.5776844,
                    notifications: 2,
                    transferable: true
                };

                //submit
                $scope.submit = function () {
                    //format tikets
                    var tickets = $scope.event.tickets.map(function (obj) {
                        var newObj = {};
                        newObj.name = obj.name;
                        newObj.quantity = obj.totalQuantity;
                        newObj.price = obj.price;
                        newObj.datetime = moment(obj.dateEnd, "YYYY-MM-DD LT").format("YYYY-MM-DD\THH:mm:ss");
                        newObj.desc = obj.description ? obj.description : "";
                        newObj.limit = obj.limitPerUser ? obj.limitPerUser : "";
                        newObj.promoterFees = obj.promoterFees ? obj.promoterFees : 0;
                        newObj.hidden = obj.hidden;

                        return newObj;
                    });

                    Upload.upload({
                        url: myConfig.backend + '/promoters-api/events/add',
                        method: 'POST',
                        file: $scope.picture,
                        sendFieldsAs: 'form',
                        fields: {
                            tickets: tickets,
                            freeTickets: $scope.event.freetickets,
                            donation: $scope.event.donation,
                            notifications: $scope.event.notifications,
                            desc: $scope.event.desc,
                            endDate: moment.isMoment($scope.event.endDate) ? $scope.event.endDate.format("YYYY-MM-DD\THH:mm:ss") : moment($scope.event.endDate).format("YYYY-MM-DD\THH:mm:ss"),
                            startDate: moment.isMoment($scope.event.startDate) ? $scope.event.startDate.format("YYYY-MM-DD\THH:mm:ss") : moment($scope.event.startDate).format("YYYY-MM-DD\THH:mm:ss"),
                            name: $scope.event.name,
                            placeAddress: $scope.event.placeAddress,
                            placeCity: $scope.event.placeCity,
                            placeDesc: $scope.event.placeDesc,
                            placeLat: $scope.event.placeLat,
                            placeLng: $scope.event.placeLng,
                            placeName: $scope.event.placeName,
                            placePostalCode: $scope.event.placePostalCode,
                            placeType: $scope.event.placeType,
                            placeURL: $scope.event.placeURL,
                            selectEventCat: $scope.event.selectEventCat.id,
                            venueHiddenSelect: $scope.event.venueHiddenSelect,
                            url: $scope.event.url,
                            link: $scope.event.link,
                            transferable: $scope.event.transferable
                        }
                    }).then(function (data) {
                        if (data.data.errors) {
                            $scope.senderror = true;
                            $scope.errors = data.data.errors;
                        } else {
                            $location.path('/review/' + data.data.data[0].id + "/success");
                        }
                    }, function (data) {
                        // handle error
                        $scope.senderror = true;
                        $scope.errors = data;
                    });
                };
            }]);
