'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AffiliatesCtrl
 * @description
 * # AffiliatesCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('AffiliatesCtrl', ["$rootScope", "$scope", "$routeParams", "Accounts", "Affiliates", "Data", "$httpParamSerializerJQLike", "Dashboard", 'fakeData', function ($rootScope, $scope, $routeParams, Accounts, Affiliates, Data, $httpParamSerializerJQLike, Dashboard, fakeData) {

                var $this = this;

                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;

                var id = $routeParams.id;
                var fake = id === 0 ? true : false;

                $scope.order = "promoter.total";

                //take data for the event that interest us
                $scope.affiliates = [];

                $this.init = function (result) {
                    $scope.ev = result.event;
                    $rootScope.roles = $scope.ev.roles;

                    $scope.affiliates = result.data;
                    $scope.links = Data.getLinkAffiliates($scope.affiliates);

                    //chart 
                    $scope.pieData = Dashboard.getPieAffiliate($scope.links);
                    $scope.piechart = {
                        "options": {
                            "chart": {
                                "type": "pie"
                            }
                        },
                        "series": [{
                                name: 'Sales',
                                colorByPoint: true,
                                data: $scope.pieData
                            }],
                        "title": {
                            "text": "Affiliate ticket sales"
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {}
                    };
                };

                //init the affiliate page
                if (!fake) {
                    Data.getAffiliates(id, $this.init);
                } else {
                    $this.init({event:fakeData.event, data:fakeData.affiliates});
                }

                //add affiliate methods
                $scope.refreshUsers = function (search) {
                    var data = {search: search};
                    return Accounts.usersSearch({}, $httpParamSerializerJQLike(data));
                };

                $scope.submit = function () {

                    if (fake) {
                        return 1;
                    }

                    //format affiliate
                    var affiliates = $scope.affiliates.map(function (obj) {
                        var newObj = {};
                        if (obj.id) {
                            newObj.id = obj.id;
                        }
                        newObj.promoter = obj.promoter.id;
                        var roles = Object.keys(obj.roles);
                        newObj.roles = roles.filter(function (roles) {
                            return obj.roles[roles];
                        });
                        newObj.link = false;
                        if (roles.indexOf("link") > 0) {
                            newObj.link = true;
                        }

                        return newObj;
                    });

                    var data = {id: id, affiliates: affiliates};
                    Affiliates.add({}, $httpParamSerializerJQLike(data))
                            .$promise.then(function (result) {
                                if (result.errors) {
                                    $scope.success = false;
                                    $scope.error = true;
                                    $scope.errors = result.errors;
                                } else {
                                    $scope.success = true;
                                    $scope.error = false;
                                    $this.init(result);
                                }
                            });
                };
            }]);
