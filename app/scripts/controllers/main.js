'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
		.controller('MainCtrl', ['$rootScope', '$scope', 'Data', 'fakeData', function ($rootScope, $scope, Data, fakeData) {
				var $this = this;

				$rootScope.dashboard = true;
				$rootScope.events = false;
				$rootScope.add = false;
				$rootScope.network = false;
				
                $scope.fake = fakeData.event;

				$this.setCharts = function (dashboard) {
					//chart options 
					//dashboard
					$scope.dashboardconfig = {
						"options": {
							"chart": {
								"type": "line"
							},
							"plotOptions": {
								"series": {
									marker: {
										fillColor: '#FFFFFF',
										lineWidth: 2,
										lineColor: null,
										symbol: 'circle'
									}
								}
							},
							legend: {
								align: 'right',
								verticalAlign: 'top',
								x: 0
							},
							xAxis: {
								gridLineWidth: 1,
								title: {
									text: null
								},
								tickWidth: 0,
								categories: dashboard.date
							},
							yAxis: {
								gridLineWidth: 0,
								labels: {
									enabled: false
								},
								title: {
									text: null
								}
							},
						},
						"series": [
							{
								"name": "Visitors",
								"data": dashboard.visitors,
								"connectNulls": false,
								"id": "visitors",
								"type": "column",
								"color": "#5e7280"
							},
							{
								"name": "Orders",
								"data": dashboard.orders,
								"type": "spline",
								"id": "orders",
								"connectNulls": true,
								"color": "#609cce"
							},
							{
								"name": "Tickets",
								"data": dashboard.tickets,
								"type": "spline",
								"id": "tickets",
								"connectNulls": true,
								"color": "#91ccff"
							}
						],
						"title": {
							"text": null
						},
						"credits": {
							"enabled": false
						},
						"loading": false,
						"size": {
							height: 475
						}
					};
				};

				Data.getEventStats(
					function (promoted) {

						if (promoted.fake) {
							promoted = fakeData.dashboard;
							$scope.listfake = true;
						}

						// numbers for the others
						$scope.totalsold = promoted.tickets.reduce(function (a, b) {
							return a + b;
						});
						$scope.coming = promoted.promoted;
						$scope.eventweek = promoted.weeks;
						$scope.profit = promoted.profits;

						//dashboard data
						$this.setCharts(promoted);
				});

				//function for infinite scroll
                $scope.evlist = [];
                $scope.infinite = false;
                $scope.empty = false;
				Data.getSellingEvents(
					function (results) {
						if (results.length > 0) {
                            $scope.infinite = false;
                            $scope.evlist = $scope.evlist.concat(results);

							console.log($scope.evlist);
                        }else{
                            $scope.empty = true;
                        }
				});
			}]);

