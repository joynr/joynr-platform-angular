'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:TicketsCtrl
 * @description
 * # TicketsCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('TicketsCtrl', ["$rootScope", "$scope", 'Data', 'Tickets', "Accounts", '$routeParams', '$interval', '$httpParamSerializer', '$httpParamSerializerJQLike', '$window', 'fakeData', 'myConfig', function ($rootScope, $scope, Data, Tickets, Accounts, $routeParams, $interval, $httpParamSerializer, $httpParamSerializerJQLike, $window, fakeData, myConfig) {
                var $this = this;
                //menu general left highlight
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;
                $scope.transferticket = false;

                // menu page hightlight
                $scope.tab = "tickets";
                
                //init var
                $scope.ticketstype = [];
                $scope.ticketsList = [];
                var ticket_to_transfer = null;
                var id = $routeParams.id;
                $scope.fake = id === 0 ? true : false;
                $scope.url = myConfig.backend;

                $this.ticketCallback = function (result) {
                    $scope.ev = result.event;
                    $rootScope.roles = $scope.ev.roles;
                    $scope.ticketsList = result.data;
                    $scope.ticketSold = Data.getTotalTicketsSold([result.event]);
                    $scope.ticketTotal = Data.getTotalTickets([result.event]);
                    $scope.ticketstype = Data.getListTypeTickets($scope.ticketsList);
                    $scope.checkedIn = 0;
                    for (var i = result.data.length - 1; i >= 0; i--) {
                        if (result.data[i].validated)
                        $scope.checkedIn++;
                    }
                };

                //create function to set data
                $scope.settickets = function () {
                    Data.getTicketsList(id, $this.ticketCallback);
                };

                $scope.Range = function (start, end) {
                    var result = [];
                    for (var i = end; i >= start; i--) {
                        result.push(i);
                    }
                    return result;
                };

                // recuperate data to show on the page when first loaded
                if (!$scope.fake) {
                    $scope.settickets();
                } else {
                    $this.ticketCallback({"event":fakeData.event, "data":fakeData.tickets});
                }
                //create interval for the polling
                /*var interval = $interval(function () {
                 $scope.settickets();
                 }, 10000);*/

                //cancel interval when we change page
                /*$scope.$on('$locationChangeStart', function () {
                 $interval.cancel(interval);
                 });*/

                //check or uncheck tickets
                $scope.check = function (ticket) {
                    if (ticket.validated) {
                        if (!$scope.fake) {
                            Tickets.resetcheckin({}, $httpParamSerializer({id: ticket.id}));
                        }
                        ticket.validated = false;
                        $scope.ticketsList.filter(function(t){
                            if(t.id == ticket.id) {
                                t.validated = false;
                            }
                        } );
                        $scope.checkedIn--;
                    } else {
                        if (!$scope.fake) {
                            Tickets.checkin({}, $httpParamSerializer({
                                id: ticket.id,
                                eventid: $scope.ev.id
                            })).$promise.then(function(response) {
                                if (response.success) {
                                    ticket.validated = true;
                                    $scope.checkedIn++;
                                    $scope.ticketsList.filter(function(t){
                                        if(t.id == ticket.id) {
                                            t.validated = true;
                                        }
                                    } );
                                } else
                                {
                                    alert(response.error);
                                }
                            });
                        }else
                        {
                            ticket.validated = true;
                            $scope.checkedIn++;
                            $scope.ticketsList.filter(function(t){
                                if(t.id == ticket.id) {
                                    t.validated = true;
                                }
                            } );
                        }
                    }
                };

                //filter ticket list by button click
                $scope.button = {};
                $scope.selected = "All";
                $scope.query = "";
                $scope.suppreslist = false;
                $scope.ticketselect = "";

                $scope.filter = function (name, key, filter, guest, id) {
                    $scope.button = {};
                    if (key !== "undefined" && filter !== "undefined") {
                        $scope.button[key] = filter;
                    }
                    $scope.query = "";
                    $scope.selected = name;
                    $scope.suppreslist = guest === 2 ? true : false;
                    $scope.ticketselect = guest === 2 ? id : 0;
                };

                //add guest
                $scope.submitguest = function (guest) {
                    return Tickets.addguest({}, $httpParamSerializer({
                        id: id,
                        name: guest.name,
                        email: guest.email,
                        tickets: guest.tickets.ticketid,
                        comment: guest.comment
                    }));
                };

                //add list
                $scope.submitlist = function (list) {
                    return Tickets.addlist({}, $httpParamSerializer({
                        id: id,
                        list: list.data
                    }));
                };

                //add quick guest
                $scope.submitquickguest = function () {
                    return Tickets.addguest({option: 1}, $httpParamSerializer({
                        id: id
                    })).$promise.then($scope.settickets());
                };

                //suppress the list
                $scope.suppresslist = function () {
                    Data.suppressList($scope.ticketselect, function () {
                        $scope.settickets();
                        $scope.filter("All");
                    });
                };

                //modal for user
                $scope.modal = function (tickets) {
                    $scope.userInModal = Data.getTicketsFromUser($scope.ticketsList, tickets.user_id);
                    $('#ticketModal').modal('show');

                    // Show modal and reset the transfer form
                    $scope.resettransferform();
                    if ($scope.transferticket)
                    $scope.transferticket = !$scope.transferticket;
                };

                //REFUNDS AND CANCELLATIONS:
                // Cancel ticket
                $scope.cancelTicket = function (ticket) {
                    if (confirm("Are you sure? This cannot be undone")) {
                        return Tickets.cancelTicket({option: 1}, $httpParamSerializer({
                            id: ticket.id
                        })).$promise.then(function(response){
                            if (response.success) {
                                ticket.price = 0;
                                $scope.ticketSold --;
                                ticket.refunded = 1;
                                // $scope.error = "All good";
                                var result  = $scope.ticketsList.filter(function(t){
                                    if(t.id == ticket.id) {
                                        t.price = 0;
                                        t.refunded = 1;
                                    }
                                } );
                                $scope.success = "Action Successful";
                            } else
                            {
                                $scope.errors = response.errors;
                            }
                            
                        });
                    }
                };

                //REFUNDS AND CANCELLATIONS:
                // Refund ticket
                $scope.refundTicket = function (ticket) {
                    if (confirm("Are you sure? This cannot be undone")) {
                        return Tickets.refundTicket({option: 1}, $httpParamSerializer({
                            id: ticket.id
                        })).$promise.then(function(response){
                            if (response.success) {
                                ticket.price = 0;
                                $scope.ticketSold --;
                                ticket.refunded = 1;
                                // $scope.error = "All good";
                                var result  = $scope.ticketsList.filter(function(t){
                                    if(t.id == ticket.id) {
                                        t.price = 0;
                                        t.refunded = 1;
                                    }
                                } );
                                $scope.success = "Action Successful";
                            } else
                            {
                                $scope.errors = response.errors;
                            }
                        });
                    }
                };

                //REFUNDS AND CANCELLATIONS:
                // Refund ticket
                $scope.resendTickets = function (id) {
                    return Tickets.resendTickets({option: 1}, $httpParamSerializer({
                        id: id
                    })).$promise.then(function(response){
                        if (response.success) {
                            $scope.success = "Action Successful";
                        } else
                        {
                            $scope.errors = response.errors;
                        }
                    });
                };

                // TRANSFER TICKET
                $scope.triggertransfer = function (ticketId) {
                    $scope.transferticket = !$scope.transferticket;
                    ticket_to_transfer = ticketId;
                };


                $scope.refreshUsers = function (search) {
                    var data = {search: search};
                    return Accounts.usersSearchExact({}, $httpParamSerializerJQLike(data));
                };


                $scope.addNewTicketLine = function (ticket) {
                    ticket = ticket[0];
                    $('#ticketModal').modal('hide');

                    // Let's adapt the view now that we received the info for the new ticket.
                    if ($scope.transferticket)
                    $scope.transferticket = !$scope.transferticket;

                    for (var i = $scope.ticketsList.length - 1; i >= 0; i--) {
                        if ($scope.ticketsList[i].id == ticket_to_transfer)
                        $scope.ticketsList.splice(i, 1);                  
                    }
                    $scope.ticketsList.unshift(ticket);
                    $scope.success_not_modal = "Transfer Successful";

                    $window.scrollTo(0, 0);

                };


                $scope.submittransfer = function (user) {
                    var data = {name: user.name, email: user.email, comment: user.comment, ticket: ticket_to_transfer};
                    return Tickets.transferTicket({}, $httpParamSerializerJQLike(data));
                };

                $this.compare = function (a,b) {
                    if (a.user_name < b.user_name)
                        return -1;
                    if (a.user_name > b.user_name)
                        return 1;
                    return 0;
                }
            }]);