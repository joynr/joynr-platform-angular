'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:DiscountsCtrl
 * @description
 * # DiscountsCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('DiscountsCtrl', ["$rootScope", "$scope", "$routeParams", "Data", "moment", "Discounts", "$httpParamSerializerJQLike", "Dashboard", 'fakeData', 'myConfig', function ($rootScope, $scope, $routeParams, Data, moment, Discounts, $httpParamSerializerJQLike, Dashboard, fakeData, myConfig) {

                var $this = this;
        
                //menu general left highlight
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;
                $scope.url = myConfig.backend;
                $scope.editMode = false;
                $scope.newDiscount = {addMode:false};
                $scope.loading = false;
                
                var id = $routeParams.id;
                var fake = id === 0 ? true : false;

                $this.init = function (result) {
                    $scope.ev = result.event;
                    $rootScope.roles = $scope.ev.roles;
                    $scope.maxdate = moment($scope.ev.dateEnd, moment.ISO_8601);
                    $scope.discounts = result.data;
                    $scope.discountstable = angular.copy($scope.discounts);
 
                    // Tickets Select
                    $scope.tickets = $scope.ev.tickets;
                    $scope.tickets.unshift({
                                "id": "",
                                "name": "All Tickets"
                            });

                    $scope.newDiscount.tickets = $scope.tickets[0];
                    $scope.newDiscount.addMode = false;
                    $scope.newDiscount.unique = false;

                    //chart
                    $this.initChart(result.data);
                };

                $this.initChart = function(discounts) {
                    $scope.pieData = Dashboard.getPieDiscounts(discounts);
                    console.log($scope.pieData);
                    for (var i = $scope.pieData.length - 1; i >= 0; i--) {
                        if( typeof $scope.pieData[i].y != 'number' ) {
                            $scope.pieData[i].y = parseInt($scope.pieData[i].y);
                        }
                    }
                    $scope.piechart = {
                        "options": {
                            "chart": {
                                "type": "pie"
                            }
                        },
                        "series": [{
                                name: 'Discounts used',
                                colorByPoint: true,
                                data: $scope.pieData
                            }],
                        "title": {
                            "text": "Affiliate ticket sales"
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {}
                    };

                    console.log($scope.discounts);
                }

                if (!fake) {
                    Data.getDiscounts(id, $this.init);
                } else {
                    $this.init({event:fakeData.event, data:fakeData.discount});
                };

                $scope.addDiscount = function (discount) {

                    var data = {id: id, discount: discount};
                    $scope.loading = true;

                    Discounts.add({}, $httpParamSerializerJQLike(data))
                        .$promise.then(function (result) {
                            $scope.loading = false;
                            if (result.errors) {
                                $scope.success = false;
                                $scope.error = true;
                                $scope.errors = result.errors;
                            } else {
                                // Update DOM
                                $scope.success = true;
                                $scope.error = false;
                                $scope.message = "Your discount has been saved";

                                // Adapt Discount Variable
                                $scope.discounts = result.data;
                                $scope.discountstable = angular.copy($scope.discounts);

                                $scope.newDiscount.tickets = $scope.tickets[0];
                                $scope.newDiscount.addMode = false;
                                $scope.newDiscount.unique = false;
                                $scope.newDiscount.code = "";
                                $scope.newDiscount.discount = false;
                                $scope.newDiscount.totalQuantity = "";

                                $this.initChart(result.data);
                            }
                        });
                }

                $scope.editDiscount = function (discount) {
                    delete discount['$$hashKey'];
                    var data = {id: id, discount: discount};
                    $scope.loading = true;

                    Discounts.edit({}, $httpParamSerializerJQLike(data))
                        .$promise.then(function (result) {
                            $scope.loading = false;
                            discount.editMode = false;
                            if (result.errors) {
                                $scope.success = false;
                                $scope.error = true;
                                $scope.errors = result.errors;
                            } else {
                                // Update DOM
                                $scope.success = true;
                                $scope.error = false;
                                $scope.message = "Your discount has been edited";
                            }
                        });
                }

                $scope.removeDiscount = function (discount, discounts, index) {
                    if (confirm("Are you sure? This cannot be undone")) {
                    
                        delete discount['$$hashKey'];
                        var data = {id: id, discount: discount};
                        $scope.loading = true;

                        Discounts.remove({}, $httpParamSerializerJQLike(data))
                            .$promise.then(function (result) {
                                $scope.loading = false;
                                discount.editMode = false;
                                if (result.errors) {
                                    $scope.success = false;
                                    $scope.error = true;
                                    $scope.errors = result.errors;
                                } else {
                                    // Update DOM
                                    $scope.success = true;
                                    $scope.error = false;
                                    $scope.message = "Your discount has been removed";

                                    discounts.splice(index, 1);
                                    $scope.discounts.splice(index, 1);
                                    $scope.discountstable.splice(index, 1);
                                    $scope.$evalAsync();

                                    $this.initChart(result.data);
                                }
                            }
                        );
                    }
                }
            }]);
