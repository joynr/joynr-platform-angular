'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('EventsCtrl', ['$rootScope', '$scope', 'Data', 'Events', '$routeParams', '$filter', 'fakeData', function ($rootScope, $scope, Data, Events, $routeParams, $filter, fakeData) {
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;

                $rootScope.date = $routeParams.date || new Date();
                
                $scope.fake = fakeData.event;
                
                if ($rootScope.admin) {
                    $rootScope.dates = Events.dates();
                }

                //call the date filter
                $scope.order = '-dateStart';
                $scope.dateEv = 0;

                //function for infinite scroll
                $scope.evlist = [];
                $scope.infinite = false;
                $scope.empty = false;
                var page = 0;
                $scope.addMoreItems = function () {
                    $scope.infinite = true;
                    if (!$rootScope.admin) {
                        Data.getEvents(false, page++).$promise.then(function (result) {
                            if (result.length > 0) {
                                $scope.infinite = false;
                                $scope.evlist = $scope.evlist.concat(result);
                            }else{
                                $scope.empty = true;
                            }
                        });
                    }else{
                         $scope.evlist = Data.getEvents($rootScope.admin, $rootScope.admin ? $filter('date')($rootScope.date, "yyyy-MM-dd") : 0);
                    }
                    
                };
                
                $scope.addMoreItems();

                $scope.search = function (query) {
                    $scope.evlist = Events.eventsSearch({offset: query});
                };

            }]);
