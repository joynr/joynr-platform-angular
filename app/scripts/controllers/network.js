'use strict';
/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:NetworkCtrl
 * @description
 * # NetworkCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('NetworkCtrl', ['$rootScope', '$scope', 'Data', 'Dashboard', 'bowser', "$window", "Groups", "Network", "$httpParamSerializerJQLike", "myConfig", "Events", function ($rootScope, $scope, Data, Dashboard, bowser, $window, Groups, Network, $httpParamSerializerJQLike, myConfig, Events) {

                var $this = this;
                $rootScope.dashboard = false;
                $rootScope.events = false;
                $rootScope.add = false;
                $rootScope.network = true;
                var browser = bowser.name;
                $scope.load = false;
                $scope.order = '-purchasedTickets.length';
                $scope.networklist = [];
                $scope.groups = [];
                $scope.revenue = 0;
                $scope.eventSelected = [];
                $scope.filters = [];
                $scope.filterslist = [];
                $scope.resultfilter = [];
                $scope.tab = "create";
                //selection of user to make group
                $scope.selection = {
                    'name': "",
                    'names': [],
                    'group': []
                };
                //create the choose of number of page select
                $scope.numberpage = [50, 100, 500];
                $scope.page = {
                    value: 50,
                    get: function () {
                        return this.value;
                    }
                };
                //get excel email
                $scope.getEmail = function () {
                    if (browser === "Safari") {
                        $window.alert("This feature is not available on Safari. Please use Firefox or Chrome.");
                        return 0;
                    }
                    
                    return Data.getExcelEmail($scope.filtered);
                };
                // Groups
                //create a comparator for group
                $scope.groupComparator = function (input, search_param) {
                    if (input && input.grouplabel) {
                        return input.groupid === search_param;
                    }
                    return false;
                };
                //click on the checkbox in th to check all the user in the list
                $scope.addAllToGroup = function () {
                    var toggleStatus = $scope.isAllSelected;
                    $scope.selection.group = [];
                    if (toggleStatus) {
                        for (var i = 0, count = $scope.filtered.length; i < count; i++) {
                            $scope.selection.group[i] = $scope.filtered[i].id;
                        }
                    }
                };
                //modal for group
                $scope.add = 0;
                $scope.modal = function (add, user) {
                    if (user) {
                        $scope.userInModal = user;
                    }
                    console.log(user);
                    $scope.add = add;
                    $('#myModal').modal('show');
                };
                //submit to the groups api
                $this.callbackSubmit = function (result) {
                    if (result.errors) {
                        $scope.success = false;
                        $scope.error = true;
                        $scope.errors = result.errors;
                    } else {
                        $scope.success = true;
                        $scope.error = false;
                        $scope.selection = {
                            'name': "",
                            'names': [],
                            'group': []
                        };
                        $scope.form.$setPristine();
                        $scope.form.$setUntouched();
                        $('#myModal').modal('hide');
                        $this.init(result.data);
                    }
                };
                $scope.submitGroup = function () {

                    //treat the data send
                    var group = $scope.selection.group.filter(Number);
                    var name = $scope.selection.name;
                    var names = $scope.selection.names;
                    var data = {
                        'name': name,
                        'group': group
                    };
                    if ($scope.add == 1) {
                        data.name = names;
                    }

                    Groups.add({param: $scope.add}, $httpParamSerializerJQLike(data))
                            .$promise.then($this.callbackSubmit);
                };
                $scope.deleteGroup = function (grp) {
                    var data = {
                        'name': $scope.selectedgroup.groupid
                    };
                    if (grp) {
                        data.group = $scope.selection.group.filter(Number);
                    } else {
                        $scope.selectedgroup = {};
                    }

                    Groups.suppress({param: grp}, $httpParamSerializerJQLike(data))
                            .$promise.then($this.callbackSubmit);
                };
                //pie chart 
                var pieData = [[], []];
                $this.getPie = function (result) {
                    $scope.revenue = Data.getTotalRevenue(result);
                    $scope.piechart.loading = true;
                    var orders = Data.getOrders(result, $scope.eventSelected);
                    var pieData = Dashboard.getPieOrigin(orders);
                    $scope.piechart.series[0].data = pieData[0];
                    $scope.piechart.options.drilldown.series = pieData[1];
                    $scope.piechart.loading = false;
                };
                $scope.piechart = {
                    title: {
                        text: ''
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.y:.1f}%'
                            }
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                    },
                    options: {
                        chart: {
                            type: 'pie'
                        },
                        drilldown: {
                            series: pieData[1]
                        },
                        legend: {
                            align: 'right',
                            x: -70,
                            verticalAlign: 'top',
                            y: 20,
                            floating: true,
                            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                            borderColor: '#CCC',
                            borderWidth: 1,
                            shadow: false
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}<br/>'
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',
                                dataLabels: {
                                    enabled: true,
                                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                                    style: {
                                        textShadow: '0 0 3px black, 0 0 3px black'
                                    }
                                }
                            }
                        }
                    },
                    series: [{
                            name: 'Origins',
                            colorByPoint: true,
                            data: pieData[0]
                        }]
                };
                //filter
                //get the list of filter
                $scope.getFilter = function () {

                    $this.fnfilterReduce = function (res, obj) {

                        if (obj.segment) {

                            var data = {
                                key: obj.filter.key,
                                data: obj.segment
                            };
                            if (obj.number) {
                                data.number = obj.number;
                            }

                            res[0].filters.push(data);
                            if (data.key === "e.id") {
                                var event = data.data;
                                res[1].push(event);
                            }
                        }

                        return res;
                    };
                    var filter = $scope.filters.reduce($this.fnfilterReduce, [{"filters": []}, []]);
                    if (filter[0].filters.length > 0) {
                        Network.filter({}, $httpParamSerializerJQLike(filter[0])).$promise.then(function (data) {
                            $scope.resultfilter = data;
                            $scope.noresultfilter = !data.length;
                        });
                        $scope.eventSelected = filter[1];
                    }
                };
                $scope.clearFilter = function () {
                    $scope.filters = [{}];
                    $scope.resultfilter = [];
                    $scope.selectedgroup = {};
                    $scope.active = false;
                };
                //refresh all data when we filter
                $this.refresh = function (newValue, oldValue) {
                    if (newValue !== oldValue) {
                        $this.getPie($scope.filtered);
                    }
                };
                $scope.$watch(function ($scope) {
                    return $scope.filtered;
                }, $this.refresh, true);
                //init
                $this.init = function (result) {
                    $scope.networklist = result;
                    $scope.events = [{id: null, name: "All"}];
                    $scope.selectedgroup = $scope.selectedgroup || {};
                    $scope.eventslist = Data.getNetworkEvents(result);
                    $scope.groupslist = Data.getGroups(result);

                    $scope.categories = Data.getCategories(result);

                    $scope.filterslist = [
                        {"filter": "Event", segment: $scope.eventslist, "key": "e.id"},
                        {"filter": "Origin", segment: myConfig.links, "key": "l.type"},
                        {"filter": "Category", segment: $scope.categories, "key": "ce.category_id"},
                        {"filter": "Attendee", segment: [{"name": "More than", "id": ">="}, {"name": "Less than", "id": "<="}], number: true, "key": "attende"}
                    ];
                    $this.getPie(result);
                };
                Data.getNetwork($this.init);
            }]);
