'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:LogoutCtrl
 * @description
 * # LogoutCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('LogoutCtrl', ["$scope", "$cookies", "$window", "myConfig", function ($scope, $cookies, $window, myConfig) {
                        //logout
                $cookies.remove("user.id");
                $cookies.remove("user.fbid");
                $cookies.remove("user.name");
                $cookies.remove("user.promoter");
               
                $window.location = myConfig.login;
            }]);
