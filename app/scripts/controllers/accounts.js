'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AccountsCtrl
 * @description
 * # AccountsCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('AccountsCtrl', ["$rootScope", "$scope", "Accounts", function ($rootScope, $scope, Accounts) {
                //menu variable
                $rootScope.dashboard = false;
                $rootScope.events = false;
                $rootScope.add = true;
                $rootScope.network = false;
                
                Accounts.query().$promise.then(function (results) {
                    $scope.accounts = results;
                });
            }]);
