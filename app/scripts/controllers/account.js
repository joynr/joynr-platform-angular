'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:AccountCtrl
 * @description
 * # AccountCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('AccountCtrl', ["$rootScope", "$scope", "User", "Upload", "myConfig", "$httpParamSerializer", "$cookies", function ($rootScope, $scope, User, Upload, myConfig, $httpParamSerializer, $cookies) {
                //menu variable
                $rootScope.dashboard = false;
                $rootScope.events = false;
                $rootScope.add = false;
                $rootScope.network = false;

                //tabs
                $scope.tab = "personal";
                $scope.user = User.get();

                $scope.submitInfo = function () {
                    $scope.infoErrors = "";

                    Upload.upload({
                        url: myConfig.backend + '/promoters-api/account/setInfo',
                        method: 'POST',
                        file: $scope.picture,
                        sendFieldsAs: 'form',
                        fields: {
                            name: $scope.user.name,
                            phone: $scope.user.phone,
                            email: $scope.user.email,
                            fbpage: $scope.user.fbpage,
                            instapage: $scope.user.insta,
                            desc: $scope.user.desc
                        }
                    }).then(function (result) {
                        if (result.errors) {
                            //if we can"t be loged we show an error
                            $scope.infoErrors = result.errors;
                        } else {
                            // we update the cookie and the root scope to show the change in the name on the interface
                            $rootScope.name = result.data.name;
                            $cookies.put("user.name", result.data.name);
                        }
                    }, function (error) {
                        // handle error
                        $scope.infoErrors = error.message;
                    });
                };

                $scope.IsMatch = true;
                $scope.passSucces = false;
                $scope.submitPassword = function () {
                    $scope.passwordErrors = "";
                    if ($scope.password.new === $scope.password.renew) {
                        User.setpassword($httpParamSerializer({current: $scope.password.current, _new: $scope.password.new, confirm: $scope.password.renew}),
                                function (result) {
                                    $scope.passSucces = true;
                                    if (result.errors) {
                                        $scope.passwordErrors = result.errors;
                                        $scope.passSucces = false;
                                    }
                                },
                                function (error) {
                                    $scope.password = {};
                                    $scope.passwordErrors = error.message;
                                });

                        //reset form
                        $scope.formpassword.$setPristine();
                        $scope.formpassword.$setUntouched();
                        $scope.password = {};
                        $scope.IsMatch = true;
                    } else {
                        $scope.IsMatch = false;
                    }
                };

                $scope.paypalSucces = false;
                $scope.submitPaypal = function () {
                    User.setpaypal($httpParamSerializer({email: $scope.user.paypalAddress}),
                            function (result) {
                                $scope.paypalSucces = true;
                                if (result.errors) {
                                    $scope.paypalErrors = result.errors;
                                    $scope.paypalSucces = false;
                                }
                            },
                            function (error) {
                                $scope.paypalErrors = error.message;
                            });
                };


            }]);
