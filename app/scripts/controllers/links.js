'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.controller:LinksCtrl
 * @description
 * # LinksCtrl
 * Controller of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .controller('LinksCtrl', ["$rootScope", "$scope", "$routeParams", "Data", "moment", "Links", "$httpParamSerializerJQLike", "Dashboard", 'fakeData', function ($rootScope, $scope, $routeParams, Data, moment, Links, $httpParamSerializerJQLike, Dashboard, fakeData) {

                var $this = this;
                //menu general left highlight
                $rootScope.dashboard = false;
                $rootScope.events = true;
                $rootScope.add = false;
                $rootScope.network = false;

                var id = $routeParams.id;
                var fake = id === 0 ? true : false;

                $this.init = function (result) {
                    $scope.ev = result.event;
                    $rootScope.roles = $scope.ev.roles;

                    $scope.maxdate = moment($scope.ev.dateEnd, moment.ISO_8601);
                    $scope.links = result.data;
                    $scope.links = result.data;
                    $scope.linkstable = angular.copy($scope.links);

                    //chart
                    var chartData = Dashboard.getChartLinks($scope.links);
                    $scope.linkschart = {
                        "options": {
                            "chart": {
                                "type": "bar"
                            },
                            "plotOptions": {
                                "series": {
                                    "stacking": ""
                                }
                            }
                        },
                        xAxis: {
                            categories: chartData[0],
                            title: {
                                text: null
                            }
                        },
                        yAxis: {
                            min: 0,
                            allowDecimals: false
                        },
                        "series": [
                            {
                                "name": "Orders",
                                "data": chartData[1]
                            },
                            {
                                "name": 'Visits',
                                "data": chartData[2]
                            }
                        ],
                        "title": {
                            "text": "Visitors and orders by links"
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {}
                    };
                };

                if (!fake) {
                    Data.getLinks(id, $this.init);
                } else {
                    $this.init({event: fakeData.event, data: fakeData.links});
                }

                $scope.submit = function () {

                    if (fake) {
                        return 1;
                    }

                    //format links to suppress all the stuff angualr add to the object
                    var links = $scope.links.map(function (obj) {
                        var newObj = {};
                        if (obj.id) {
                            newObj.id = obj.id;
                        }
                        newObj.name = obj.name;
                        newObj.type = obj.type;

                        return newObj;
                    });

                    var data = {id: id, links: links};
                    Links.add({}, $httpParamSerializerJQLike(data))
                            .$promise.then(function (result) {
                                if (result.errors) {
                                    $scope.success = false;
                                    $scope.error = true;
                                    $scope.errors = result.errors;
                                } else {
                                    $scope.success = true;
                                    $scope.error = false;
                                    $this.init(result.data);
                                }
                            });
                };
            }]);
