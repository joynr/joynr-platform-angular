'use strict';

/**
 * @ngdoc function
 * @name joynrPromotersAngularApp.decorator:Datetimepickerdirective
 * @description
 * # Datetimepickerdirective
 * Decorator of the joynrPromotersAngularApp
 */
angular.module('joynrPromotersAngularApp')
        .config(function ($provide) {
            $provide.decorator('datetimepickerDirective', ['$delegate', function ($delegate) {
                    var directive = $delegate[0];
                    var link = directive.link;

                    directive.compile = function () {
                        return function () {
                            link.apply(this, arguments);
                        };
                    };

                    return $delegate;

                }]);
        });