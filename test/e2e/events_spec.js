'use strict';

describe('Controller: EventsCtrl', function() {

    beforeEach(function() {
        browser.get('/#/events');
    });
    var evList = element.all(by.repeater('ev in list'));
    var query = element(by.model('query'));

    it('should filter the events list as a user types into the search box', function() {
        expect(evList.count()).toBe(6);
        query.sendKeys('free');
        expect(evList.count()).toBe(2);
        query.clear();
    });

    it('should be possible to control events order via the buttons', function() {

        var evListColumn = element.all(by.repeater('ev in list').column('ev.name'));
        var query = element(by.model('query'));
       
        function getNames() {
            return evListColumn.map(function(elm) {
                return elm.getText();
            });
        }
        
        query.sendKeys('free'); //let's narrow the dataset to make the test assertions shorter

        //we test only one button because selenium is way tooooooo lang to test things.
        element(by.css('.btn.red')).click();
        expect(getNames()).toEqual(['BENNY GLASGOW (ART DEPARTEMENT) FREE GUEST LIST','FREE']);
    });

});
