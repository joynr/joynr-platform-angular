'use strict';

describe('Controller: EventsCtrl', function() {

   it('should redirect index.html to index.html#/dashboard', function() {
    browser.get('/');
   
    browser.getLocationAbsUrl().then(function(url) {
        expect(url).toEqual('/dashboard');
      });
  });
});
