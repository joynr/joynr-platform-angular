'use strict';

describe('Service: groupsservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var groupsservice;
  beforeEach(inject(function (_groupsservice_) {
    groupsservice = _groupsservice_;
  }));

  it('should do something', function () {
    expect(!!groupsservice).toBe(true);
  });

});
