'use strict';

describe('Service: SpinnerServices', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var SpinnerServices;
  beforeEach(inject(function (_SpinnerServices_) {
    SpinnerServices = _SpinnerServices_;
  }));

  it('should do something', function () {
    expect(!!SpinnerServices).toBe(true);
  });

});
