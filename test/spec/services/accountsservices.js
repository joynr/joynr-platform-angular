'use strict';

describe('Service: accountsservices', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var accountsservices;
  beforeEach(inject(function (_accountsservices_) {
    accountsservices = _accountsservices_;
  }));

  it('should do something', function () {
    expect(!!accountsservices).toBe(true);
  });

});
