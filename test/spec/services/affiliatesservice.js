'use strict';

describe('Service: affiliatesservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var affiliatesservice;
  beforeEach(inject(function (_affiliatesservice_) {
    affiliatesservice = _affiliatesservice_;
  }));

  it('should do something', function () {
    expect(!!affiliatesservice).toBe(true);
  });

});
