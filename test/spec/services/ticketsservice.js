'use strict';

describe('Service: ticketsservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var ticketsservice;
  beforeEach(inject(function (_ticketsservice_) {
    ticketsservice = _ticketsservice_;
  }));

  it('should do something', function () {
    expect(!!ticketsservice).toBe(true);
  });

});
