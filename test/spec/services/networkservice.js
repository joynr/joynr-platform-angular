'use strict';

describe('Service: networkservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var networkservice;
  beforeEach(inject(function (_networkservice_) {
    networkservice = _networkservice_;
  }));

  it('should do something', function () {
    expect(!!networkservice).toBe(true);
  });

});
