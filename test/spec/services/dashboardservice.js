'use strict';

describe('Service: dashboardservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var dashboardservice;
  beforeEach(inject(function (_dashboardservice_) {
    dashboardservice = _dashboardservice_;
  }));

  it('should do something', function () {
    expect(!!dashboardservice).toBe(true);
  });

});
