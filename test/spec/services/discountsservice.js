'use strict';

describe('Service: discountsservice', function () {

  // load the service's module
  beforeEach(module('joynrPromotersAngularApp'));

  // instantiate service
  var discountsservice;
  beforeEach(inject(function (_discountsservice_) {
    discountsservice = _discountsservice_;
  }));

  it('should do something', function () {
    expect(!!discountsservice).toBe(true);
  });

});
