'use strict';

describe('Directive: discounttabledirective', function () {

  // load the directive's module
  beforeEach(module('joynrPromotersAngularApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<discounttabledirective></discounttabledirective>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the discounttabledirective directive');
  }));
});
