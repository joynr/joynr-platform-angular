'use strict';

describe('Directive: addlistdirective', function () {

  // load the directive's module
  beforeEach(module('joynrPromotersAngularApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<addlistdirective></addlistdirective>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the addlistdirective directive');
  }));
});
