'use strict';

describe('Filter: renderhtmlfitlter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var renderhtmlfitlter;
  beforeEach(inject(function ($filter) {
    renderhtmlfitlter = $filter('renderhtmlfitlter');
  }));

  it('should return the input prefixed with "renderhtmlfitlter filter:"', function () {
    var text = 'angularjs';
    expect(renderhtmlfitlter(text)).toBe('renderhtmlfitlter filter: ' + text);
  });

});
