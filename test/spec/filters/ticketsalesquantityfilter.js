'use strict';

describe('Filter: ticketsalesquantityfilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var ticketsalesquantityfilter;
  beforeEach(inject(function ($filter) {
    ticketsalesquantityfilter = $filter('ticketsalesquantityfilter');
  }));

  it('should return the input prefixed with "ticketsalesquantityfilter filter:"', function () {
    var text = 'angularjs';
    expect(ticketsalesquantityfilter(text)).toBe('ticketsalesquantityfilter filter: ' + text);
  });

});
