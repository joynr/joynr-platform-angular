'use strict';

describe('Filter: eventslinkfilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var eventslinkfilter;
  beforeEach(inject(function ($filter) {
    eventslinkfilter = $filter('eventslinkfilter');
  }));

  it('should return the input prefixed with "eventslinkfilter filter:"', function () {
    var text = 'angularjs';
    expect(eventslinkfilter(text)).toBe('eventslinkfilter filter: ' + text);
  });

});
