'use strict';

describe('Filter: favcategoryfilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var favcategoryfilter;
  beforeEach(inject(function ($filter) {
    favcategoryfilter = $filter('favcategoryfilter');
  }));

  it('should return the input prefixed with "favcategoryfilter filter:"', function () {
    var text = 'angularjs';
    expect(favcategoryfilter(text)).toBe('favcategoryfilter filter: ' + text);
  });

});
