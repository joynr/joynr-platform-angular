'use strict';

describe('Filter: slugfilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var slugfilter;
  beforeEach(inject(function ($filter) {
    slugfilter = $filter('slugfilter');
  }));

  it('should return the input prefixed with "slugfilter filter:"', function () {
    var text = 'angularjs';
    expect(slugfilter(text)).toBe('slugfilter filter: ' + text);
  });

});
