'use strict';

describe('Filter: linktypefilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var linktypefilter;
  beforeEach(inject(function ($filter) {
    linktypefilter = $filter('linktypefilter');
  }));

  it('should return the input prefixed with "linktypefilter filter:"', function () {
    var text = 'angularjs';
    expect(linktypefilter(text)).toBe('linktypefilter filter: ' + text);
  });

});
