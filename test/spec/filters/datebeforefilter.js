'use strict';

describe('Filter: dateBeforeFilter', function () {

  // load the filter's module
  beforeEach(module('dateBeforeFilter'));

  // initialize a new instance of the filter before each test
  var dateBefore;
  beforeEach(inject(function ($filter) {
    dateBefore = $filter('dateBefore');
  }));

  it('should return true because the date is before now', function () {
    var date = new Date();
    date = date.setDate(date.getDate() - 6); 
    expect(dateBefore(date.toString()).toBe(true));
  });
  
  it('should return fale because the date is after now', function () {
    var date = new Date();
    date = date.setDate(date.getDate() + 6); 
    expect(dateBefore(date.toString())).toBe(false);
  });

});
