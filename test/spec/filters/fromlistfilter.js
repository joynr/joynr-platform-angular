'use strict';

describe('Filter: fromListFilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var fromListFilter;
  beforeEach(inject(function ($filter) {
    fromListFilter = $filter('fromListFilter');
  }));

  it('should return the input prefixed with "fromListFilter filter:"', function () {
    var text = 'angularjs';
    expect(fromListFilter(text)).toBe('fromListFilter filter: ' + text);
  });

});
