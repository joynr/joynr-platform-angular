'use strict';

describe('Filter: ticketsfilter', function () {

  // load the filter's module
  beforeEach(module('joynrPromotersAngularApp'));

  // initialize a new instance of the filter before each test
  var ticketsfilter;
  beforeEach(inject(function ($filter) {
    ticketsfilter = $filter('ticketsfilter');
  }));

  it('should return the input prefixed with "ticketsfilter filter:"', function () {
    var text = 'angularjs';
    expect(ticketsfilter(text)).toBe('ticketsfilter filter: ' + text);
  });

});
